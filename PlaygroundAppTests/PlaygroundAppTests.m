//
//  PlaygroundAppTests.m
//  PlaygroundAppTests
//
//  Created by James Watmuff on 27/02/13.
//  Copyright (c) 2013 Planticle. All rights reserved.
//

#import "PlaygroundAppTests.h"

@implementation PlaygroundAppTests

- (void)setUp
{
    [super setUp];
    
    // Set-up code here.
}

- (void)tearDown
{
    // Tear-down code here.
    
    [super tearDown];
}

- (void)testExample
{
    STFail(@"Unit tests are not implemented yet in PlaygroundAppTests");
}

@end
