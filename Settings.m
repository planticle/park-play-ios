//
//  Settings.m
//  PlaygroundApp
//
//  Created by James Watmuff on 20/05/13.
//  Copyright (c) 2013 Planticle. All rights reserved.
//

#import "Settings.h"
#import "Playground.h"
#import "PAData.h"

@implementation Settings

@synthesize sortMode;
@synthesize sortPin;
@synthesize selectedFacilities;
@synthesize selectedEquipment;
@synthesize favouriteSelected;
@synthesize ourFavouriteSelected;

-(id)init
{
    self = [super init];
    if(self) {
        [self reset];
        sortPin = CLLocationCoordinate2DMake(-34.9333, 138.5833);
    }
    return self;
}

- (NSSet *)selectPlaygrounds
{
    if(favouriteSelected) {
        return [[PAData sharedInstance] favouritePlaygrounds];
    } else if(ourFavouriteSelected) {
        return [[PAData sharedInstance] ourFavouritePlaygrounds];
    } else {
        return [[PAData sharedInstance] playgroundsWithFacilities:selectedFacilities andEquipment:selectedEquipment];
    }
}

-(CLLocation *)sortPinLocation
{
    return [[CLLocation alloc] initWithLatitude:sortPin.latitude longitude:sortPin.longitude];
}

-(void)reset
{
    sortMode = kDistanceFromMe;
    selectedEquipment = [[NSMutableSet alloc] init];
    selectedFacilities = [[NSMutableSet alloc] init];
    favouriteSelected = NO;
    ourFavouriteSelected = NO;
}

@end
