//
//  Settings.h
//  PlaygroundApp
//
//  Created by James Watmuff on 20/05/13.
//  Copyright (c) 2013 Planticle. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

typedef NS_ENUM(NSUInteger, PlaygroundSortMode) {
    kDistanceFromMe,
    kDistanceFromPin,
    kAlphabetical
};

@interface Settings : NSObject

@property (nonatomic) PlaygroundSortMode sortMode;
@property (nonatomic) CLLocationCoordinate2D sortPin;
@property (nonatomic, strong) NSMutableSet *selectedFacilities;
@property (nonatomic, strong) NSMutableSet *selectedEquipment;
@property (nonatomic, assign) BOOL favouriteSelected;
@property (nonatomic, assign) BOOL ourFavouriteSelected;

- (NSSet *)selectPlaygrounds;
- (CLLocation *)sortPinLocation;
- (void)reset;
@end
