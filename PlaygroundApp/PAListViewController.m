//
//  PAListViewController.m
//  PlaygroundApp
//
//  Created by James Watmuff on 27/02/13.
//  Copyright (c) 2013 Planticle. All rights reserved.
//

#import "PAListViewController.h"
#import "PADetailInfoViewController.h"
#import "Playground+Extra.h"
#import "Facility.h"
#import "Equipment.h"
#import "PlaygroundFacility.h"
#import "PlaygroundEquipment.h"
#import "PAUtil.h"
#import "PADataImporter.h"
#import "PAData.h"

@interface PAListViewController () {
    NSSet *lastSelectedFacilities;
    NSSet *lastSelectedEquipment;
    BOOL lastFavouriteSelected;
    BOOL lastOurFavouriteSelected;
    NSArray *playgrounds;
    
    // Alphabetical index
    NSArray *sectionOffsets;
    NSArray *sectionLengths;
}

@end

@implementation PAListViewController

@synthesize mainViewController;
@synthesize settings;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    UIImage *background = [UIImage imageNamed:@"images/tableview-bg.png"];
    self.view.backgroundColor = [UIColor colorWithPatternImage:background];
    self.tableView.separatorColor = [UIColor clearColor];

    if([[[UIDevice currentDevice] systemVersion] doubleValue] >= 7) {
        [[self tableView] setSectionIndexColor:[UIColor grayColor]];
        [[self tableView] setSectionIndexBackgroundColor:[UIColor clearColor]];
    }
}

- (void)viewWillAppear:(BOOL)animated
{
    CGRect frame = self.parentViewController.view.bounds;
    
    // hack to account for additional UIToolbar
    frame.origin.y += 44;
    frame.size.height -=44;
    
    self.view.frame = frame;

    [self refreshIfFiltersChanged];
    
    [super viewWillAppear:animated];
}

- (void)viewWillLayoutSubviews
{
     // iOS 7 Adjustments
     if([[[UIDevice currentDevice] systemVersion] doubleValue] >= 7) {
         CGRect frame = self.parentViewController.view.bounds;
         id<UILayoutSupport> topLayoutGuide = [self.parentViewController topLayoutGuide];
         frame.origin.y += ([topLayoutGuide length] + 44);
         frame.size.height -= ([topLayoutGuide length] + 44);
         self.view.frame = frame;
         self.tableView.contentInset = UIEdgeInsetsZero;
         self.tableView.scrollIndicatorInsets = UIEdgeInsetsZero;
     }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)refreshIfFiltersChanged
{
    //if([settings.selectedFacilities isEqualToSet:lastSelectedFacilities] && [settings.selectedEquipment isEqualToSet:lastSelectedEquipment] && settings.favouriteSelected == lastFavouriteSelected && settings.ourFavouriteSelected == lastOurFavouriteSelected) return;

    lastSelectedFacilities = [[NSSet alloc] initWithSet:settings.selectedFacilities];
    lastSelectedEquipment = [[NSSet alloc] initWithSet:settings.selectedEquipment];
    lastFavouriteSelected = settings.favouriteSelected;
    lastOurFavouriteSelected = settings.ourFavouriteSelected;

    [self updatePlaygrounds];

    NSIndexPath *selectedPath = [self.tableView indexPathForSelectedRow];
    [self.tableView reloadData];
    [self.tableView selectRowAtIndexPath:selectedPath animated:NO scrollPosition:UITableViewScrollPositionNone];
}

- (CLLocationManager *) getLocationManager
{
    if(locationManager == nil) {
        locationManager = [[CLLocationManager alloc] init];
    }
    
    locationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters;
    locationManager.distanceFilter = 200.0f;
    locationManager.delegate = self;
    
    return locationManager;
}

- (void)updatePlaygrounds
{
    // Fetch playgrounds
    NSSortDescriptor *descriptor = [NSSortDescriptor sortDescriptorWithKey:@"name" ascending:YES selector:@selector(localizedCaseInsensitiveCompare:)];
    playgrounds = [[settings selectPlaygrounds] sortedArrayUsingDescriptors:@[descriptor]];
    
    // Sort by distance from pin
    NSInteger sortMode = settings.sortMode;
    if(sortMode == kDistanceFromMe) {
        [[self getLocationManager] startUpdatingLocation];
        playgrounds = [PAUtil sortPlaygrounds:playgrounds byDistanceFromLocation:locationManager.location];
    } else {
        if(locationManager != nil)
            [locationManager stopUpdatingLocation];
    }
    
    if(sortMode == kDistanceFromPin) {
        playgrounds = [PAUtil sortPlaygrounds:playgrounds byDistanceFromLocation:settings.sortPinLocation];
    }
    
    if(sortMode == kAlphabetical) {
        [self calculateSections];
    }
}

- (void) calculateSections
{
    NSInteger sectionIndex = -1;
    
    NSString *sectionIndicies = @"ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    NSMutableArray *offsets = [[NSMutableArray alloc] initWithCapacity:26];
    NSMutableArray *lengths = [[NSMutableArray alloc] initWithCapacity:26];

    NSInteger i = 0;
    for(Playground *playground in playgrounds) {
        NSString *firstChar = [[playground.name substringToIndex:1] uppercaseString];
        NSInteger newSectionIndex = [sectionIndicies rangeOfString:firstChar].location;
        while(sectionIndex < newSectionIndex) {
            sectionIndex++;
            offsets[sectionIndex] = @(i);
            lengths[sectionIndex] = @(0);
        }
        lengths[sectionIndex] = @([lengths[sectionIndex] intValue] + 1);
        i++;
    }
    
    while(sectionIndex < 26) {
        sectionIndex++;
        offsets[sectionIndex] = @(i);
        lengths[sectionIndex] = @(0);
    }
    
    sectionOffsets = [NSArray arrayWithArray:offsets];
    sectionLengths = [NSArray arrayWithArray:lengths];
}

- (NSString *) distanceStringForPlayground:(Playground *)playground
{
    CLLocation *origin;
    if(settings.sortMode == kDistanceFromPin) {
        origin = settings.sortPinLocation;
    } else {
        origin = locationManager.location;
    }
    
    if(origin == nil) {
        return @"--- km";
    }
    
    CLLocationDistance distance = [playground.location distanceFromLocation:origin]/1000.0;
    if(distance < 5.5) {
        return [NSString stringWithFormat:@"%.1fkm", distance];
    } else {
        return [NSString stringWithFormat:@"%dkm", (int)(distance + 0.5)];
    }
}

#pragma mark - CLLocationManagerDelegate

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    playgrounds = [PAUtil sortPlaygrounds:playgrounds byDistanceFromLocation:locations.lastObject];
    [self.tableView reloadData];
}

#pragma mark -
#pragma mark Table view data source methods

/*
 The data source methods are handled primarily by the fetch results controller
 */

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if([self isAlphaSorted]) {
        return 26;
    } else {
        return 1;
    }
}


// Customize the number of rows in the table view.
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if([self isAlphaSorted]) {
        return [sectionLengths[section] integerValue];
    } else {
        return MAX(1, playgrounds.count);
    }
}

- (NSInteger)tableView:(UITableView *)tableView sectionForSectionIndexTitle:(NSString *)title atIndex:(NSInteger)index
{
    return index;
}

- (NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView
{
    if([self isAlphaSorted]) {
        return [@"A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z" componentsSeparatedByString:@","];
    } else {
        return nil;
    }
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"PlaygroundCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];

    UILabel *mainLabel;
    UILabel *addressLabel;
    UILabel *distanceLabel;
    UIImageView *iconView;
    UIImageView *favouriteIconView;
    int i;
    int numberOfIcons = [self isAlphaSorted] ? 9 : 10;
    
    if(playgrounds.count == 0) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
        if(settings.favouriteSelected) {
            cell.textLabel.text = @"You don't have any favourite playgrounds! Add a playground to your favourites from any playground detail screen.";
        } else {
            cell.textLabel.text = @"There are no playgrounds matching your criteria.";
        }
        cell.textLabel.font = [UIFont fontWithName:@"Helvetica" size:16.0];
        cell.textLabel.textColor = [UIColor darkGrayColor];
        cell.textLabel.numberOfLines = 3;
        cell.textLabel.lineBreakMode = NSLineBreakByWordWrapping;
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    }
    
    if(cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        
        mainLabel = [[UILabel alloc] init];
        mainLabel.tag = 1;
        mainLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:20.0];
        mainLabel.shadowColor = [UIColor colorWithRed:1 green:1 blue:1 alpha:0.25];
        mainLabel.shadowOffset = CGSizeMake(0, 1);
        mainLabel.backgroundColor = [UIColor clearColor];
        mainLabel.autoresizingMask = UIViewAutoresizingFlexibleWidth;
        [cell.contentView addSubview:mainLabel];
        
        addressLabel = [[UILabel alloc] init];
        addressLabel.tag = 2;
        addressLabel.font = [UIFont fontWithName:@"Helvetica" size:14.0];
        addressLabel.shadowColor = [UIColor colorWithRed:1 green:1 blue:1 alpha:0.25];
        addressLabel.shadowOffset = CGSizeMake(0, 1);
        addressLabel.backgroundColor = [UIColor clearColor];
        addressLabel.autoresizingMask = UIViewAutoresizingFlexibleWidth;
        [cell.contentView addSubview:addressLabel];
        
        distanceLabel = [[UILabel alloc] init];
        distanceLabel.tag = 3;
        distanceLabel.font = [UIFont fontWithName:@"Helvetica" size:14.0];
        distanceLabel.shadowColor = [UIColor colorWithRed:1 green:1 blue:1 alpha:0.25];
        distanceLabel.shadowOffset = CGSizeMake(0, 1);
        distanceLabel.backgroundColor = [UIColor clearColor];
        distanceLabel.textAlignment = NSTextAlignmentRight;
        distanceLabel.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin;
        [cell.contentView addSubview:distanceLabel];
        
        favouriteIconView = [[UIImageView alloc] initWithFrame:CGRectZero];
        favouriteIconView.tag = 4;
        favouriteIconView.image = [UIImage imageNamed:@"images/favourite_small"];
        favouriteIconView.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin;
        [cell.contentView addSubview:favouriteIconView];
        
        for(i = 0; i < numberOfIcons; i++) {
            iconView = [[UIImageView alloc] initWithFrame:CGRectMake(7 + i * 31, 52, 25, 25)];
            iconView.tag = i + 5;
            [cell.contentView addSubview:iconView];
        }

        cell.backgroundColor = [UIColor clearColor];
    } else {
        mainLabel = (UILabel *)[cell.contentView viewWithTag:1];
        addressLabel = (UILabel *)[cell.contentView viewWithTag:2];
        distanceLabel = (UILabel *)[cell.contentView viewWithTag:3];
        favouriteIconView = (UIImageView *)[cell.contentView viewWithTag:4];
    }
    
    Playground *playground = [self playgroundAtIndexPath:indexPath];
    
    mainLabel.text = playground.name;
    addressLabel.text = playground.address;
    distanceLabel.text = [self distanceStringForPlayground:playground];
    favouriteIconView.hidden = !playground.favourite.boolValue;
    
    float cellWidth = cell.contentView.frame.size.width;
    CGSize size = [distanceLabel.text sizeWithFont:distanceLabel.font];
    mainLabel.frame = CGRectMake(10.0, 5.0, cellWidth - 25.0 - size.width, 25.0);
    addressLabel.frame = CGRectMake(10.0, 30.0, cellWidth - 40.0, 20.0);
    distanceLabel.frame = CGRectMake(cellWidth - 10.0 - size.width, 5.0, size.width, 25.0);
    favouriteIconView.frame = CGRectMake(cellWidth - 29, 28, 21, 22);
    
    i = 0;
    NSArray *facilities = [playground.facilities sortedArrayUsingDescriptors:@[[NSSortDescriptor sortDescriptorWithKey:@"facility.name" ascending:YES]]];
    for(PlaygroundFacility *pf in facilities) {
        if(i >= numberOfIcons) break;
        iconView = (UIImageView *)[cell.contentView viewWithTag:i+5];
        iconView.image = [PAUtil iconForFacility:pf.facility];
        i++;
    }
    NSArray *equipment = [playground.equipment sortedArrayUsingDescriptors:@[[NSSortDescriptor sortDescriptorWithKey:@"equipment.name" ascending:YES]]];
    for(PlaygroundEquipment *pe in equipment) {
        if(i >= numberOfIcons) break;
        iconView = (UIImageView *)[cell.contentView viewWithTag:i+5];
        iconView.image = [PAUtil iconForEquipment:pe.equipment];
        i++;
    }


    while(i < 10) {
        iconView = (UIImageView *)[cell.contentView viewWithTag:i+5];
        iconView.image = nil;
        i++;
    }

    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 85.0;
}

#pragma mark -
#pragma mark Table view selection

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(playgrounds.count == 0) return;

    // Navigation logic may go here. Create and push another view controller.
    PADetailInfoViewController *detailViewController = [[PADetailInfoViewController alloc] initWithStyle:UITableViewStyleGrouped];
    detailViewController.playground = [self playgroundAtIndexPath:indexPath];

//    [self.navigationController.navigationBar setBackgroundImage:[PAUtil imageWithColor:[UIColor redColor]] forBarMetrics:UIBarMetricsDefault];
    
    [self.mainViewController.navigationController pushViewController:detailViewController animated:YES];
}

- (Playground *)playgroundAtIndexPath:(NSIndexPath *)indexPath
{
    if([self isAlphaSorted]) {
        return playgrounds[[sectionOffsets[indexPath.section] intValue] + indexPath.row];
    } else {
        return playgrounds[indexPath.row];
    }
}

- (BOOL)isAlphaSorted
{
    return settings.sortMode == kAlphabetical && playgrounds.count > 0;
}

@end
