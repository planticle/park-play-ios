//
//  PAFilterViewController.m
//  PlaygroundApp
//
//  Created by James Watmuff on 5/03/13.
//  Copyright (c) 2013 Planticle. All rights reserved.
//

#import "PAFilterViewController.h"
#import "Equipment.h"
#import "Facility.h"
#import "PADataImporter.h"
#import "PAData.h"
#import "Playground.h"
#import "PlaygroundFacility.h"
#import "PlaygroundEquipment.h"
#import "PAUtil.h"

typedef NS_ENUM(NSUInteger, FilterSectionType) {
    kFavourite,
    kFacilities,
    kEquipment
};

@interface PAFilterViewController () {
    NSArray *facilities;
    NSArray *equipment;
    NSArray *selectableFacilities;
    NSArray *selectableEquipment;
    BOOL favouritesExist;
}

@end

@implementation PAFilterViewController

@synthesize selectedFacilities;
@synthesize selectedEquipment;
@synthesize favouriteSelected;
@synthesize ourFavouriteSelected;
@synthesize delegate;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    UIBarButtonItem *rightButton = [PAUtil barButtonItemWithTitle:@"Done" target:self action:@selector(didPressDoneButton)];
    
    self.navigationItem.rightBarButtonItem = rightButton;
    
    facilities = [self loadAll:@"Facility" sortedBy:@"name" excludingUnselectable:NO error:nil];
    equipment = [self loadAll:@"Equipment" sortedBy:@"name" excludingUnselectable:NO error:nil];

    [self updateClearButtonAnimated:NO];
    [self updateSelectable];
    [self updateFavouritesExist];

    self.title = @"Filter";
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (NSArray*)loadAll:(NSString *)entityName sortedBy:(NSString*)sortField excludingUnselectable:(BOOL)filter error:(NSError*)error
{
    PAData *data = [PAData sharedInstance];
    
    NSMutableSet *items;
    if([entityName isEqualToString:@"Facility"]) {
        if(filter) {
            items = [[NSMutableSet alloc] initWithCapacity:data.facilities.count];
            for(Playground *playground in [data playgroundsWithFacilities:selectedFacilities andEquipment:selectedEquipment]) {
                [items unionSet:playground.allFacilities];
            }
        } else {
            items = [[NSMutableSet alloc] initWithSet:data.facilities];
        }
    } else if([entityName isEqualToString:@"Equipment"]){
        if(filter) {
            items = [[NSMutableSet alloc] initWithCapacity:data.equipment.count];
            for(Playground *playground in [data playgroundsWithFacilities:selectedFacilities andEquipment:selectedEquipment]) {
                [items unionSet:playground.allEquipment];
            }
        } else {
            items = [[NSMutableSet alloc] initWithSet:data.equipment];
        }
    } else {
        items = [[NSMutableSet alloc] init];
    }
    
    NSSortDescriptor *descriptor = [[NSSortDescriptor alloc] initWithKey:sortField ascending:YES];
    return [items sortedArrayUsingDescriptors:@[descriptor]];
}

- (void)updateSelectable
{
    selectableFacilities = [self loadAll:@"Facility" sortedBy:@"name" excludingUnselectable:YES error:nil];
    selectableEquipment = [self loadAll:@"Equipment" sortedBy:@"name" excludingUnselectable:YES error:nil];
}

- (void)updateClearButtonAnimated:(BOOL)animated
{
    BOOL showClear = (!favouriteSelected) && (!ourFavouriteSelected) && (selectedEquipment.count > 0 || selectedFacilities.count > 0);
    BOOL clearIsShown = self.navigationItem.leftBarButtonItem != nil;

    if(showClear && !clearIsShown) {
        UIBarButtonItem *clearButton = [PAUtil barButtonItemWithTitle:@"Reset" target:self action:@selector(didPressClearButton)];
        [self.navigationItem setLeftBarButtonItem:clearButton animated:animated];
    }
    
    if(!showClear && clearIsShown) {
        [self.navigationItem setLeftBarButtonItem:nil animated:animated];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)didPressDoneButton
{
    [self.delegate filterViewControllerDidFinish:self];
}

- (void)didPressClearButton
{
    [selectedEquipment removeAllObjects];
    [selectedFacilities removeAllObjects];
    [self updateClearButtonAnimated:YES];
    [self updateSelectable];
    [self.tableView reloadData];
}

- (void)updateFavouritesExist
{
    favouritesExist = NO;
    for(Playground *playground in [PAData sharedInstance].playgrounds) {
        if(playground.favourite.boolValue) {
            favouritesExist = YES;
            break;
        }
    }
    
    if(!favouritesExist) {
        favouriteSelected = NO;
    }
}

#pragma mark - Table view data source

- (FilterSectionType)typeOfSection:(NSInteger)section
{
    return (FilterSectionType)section;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    if(favouriteSelected || ourFavouriteSelected) return 1;
    else return 3;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    switch([self typeOfSection:section]) {
        case kFavourite:
            return (favouritesExist && !favouriteSelected && !ourFavouriteSelected) ? 2 : 1;
        case kFacilities:
            return facilities.count;
        case kEquipment:
            return equipment.count;
        default:
            return 0;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"FilterCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];

    if(cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }

    Facility *facility;
    Equipment *equip;
    
    NSString *resource;
    UISwitch *favouriteSwitch;
    
    BOOL ourFavourites;
    
    switch([self typeOfSection:indexPath.section]) {
        case kFavourite:
            ourFavourites = indexPath.row == 1 || !favouritesExist || ourFavouriteSelected;
            
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
            favouriteSwitch = [[UISwitch alloc] initWithFrame:CGRectZero];
            favouriteSwitch.on = ourFavourites ? ourFavouriteSelected : favouriteSelected;
            favouriteSwitch.onTintColor = [PAUtil orange];
            favouriteSwitch.tag = ourFavourites;
            [favouriteSwitch addTarget:self action:@selector(didToggleFavouriteSwitch:) forControlEvents:UIControlEventValueChanged];
            cell.accessoryView = favouriteSwitch;
            cell.textLabel.text = ourFavourites ? @"Park Play's" : @"My Favourites";
            cell.textLabel.textColor = [UIColor blackColor];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            resource = @"images/icons/favourite";
            break;
        case kFacilities:
            facility = facilities[indexPath.row];
            cell.textLabel.text = facility.desc;
            cell.accessoryType = [selectedFacilities containsObject:facility] ? UITableViewCellAccessoryCheckmark : UITableViewCellAccessoryNone;
            cell.textLabel.textColor = [selectableFacilities containsObject:facility] ? [UIColor blackColor] : [UIColor grayColor];
            
            resource = [NSString stringWithFormat:@"images/icons/%@", facility.name];
            break;
        case kEquipment:
            equip = equipment[indexPath.row];
            cell.textLabel.text = equip.desc;
            cell.accessoryType = [selectedEquipment containsObject:equip] ? UITableViewCellAccessoryCheckmark : UITableViewCellAccessoryNone;
            cell.textLabel.textColor = [selectableEquipment containsObject:equip] ? [UIColor blackColor] : [UIColor grayColor];
            
            resource = [NSString stringWithFormat:@"images/icons/%@", equip.name];
            break;
    }
    
    UIImage *theImage = [UIImage imageNamed:resource];
    cell.imageView.image = theImage;

    
    return cell;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    switch([self typeOfSection:section]) {
        case kFavourite: return @"Favourites";
        case kFacilities: return @"Facilities";
        case kEquipment: return @"Equipment";
        default: return nil;
    }
}

- (void)didToggleFavouriteSwitch:(UISwitch *)favouriteSwitch
{
    BOOL *value = favouriteSwitch.tag ? &ourFavouriteSelected : &favouriteSelected;
    
    if(*value == favouriteSwitch.on) return;
    
    *value = favouriteSwitch.on;
    
    [self.tableView beginUpdates];
    
    if(favouriteSwitch.on) {
        [self.tableView deleteSections:[NSIndexSet indexSetWithIndexesInRange:NSMakeRange(1,2)]
                      withRowAnimation:UITableViewRowAnimationFade];
        if(favouritesExist) {
            [self.tableView deleteRowsAtIndexPaths:@[[NSIndexPath indexPathForItem:favouriteSwitch.tag ? 0 : 1 inSection:kFavourite]]
                                  withRowAnimation:UITableViewRowAnimationFade];
        }
    } else {
        [self.tableView insertSections:[NSIndexSet indexSetWithIndexesInRange:NSMakeRange(1,2)]
                      withRowAnimation:UITableViewRowAnimationFade];
        if(favouritesExist) {
            [self.tableView insertRowsAtIndexPaths:@[[NSIndexPath indexPathForItem:favouriteSwitch.tag ? 0 : 1 inSection:kFavourite]]
                                  withRowAnimation:UITableViewRowAnimationFade];
        }
    }
    
    [self.tableView endUpdates];
    
    [self updateClearButtonAnimated:YES];
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    id selectedObject;
    NSMutableSet *selectionSet;
    NSArray *selectableSet;
    
    switch([self typeOfSection:indexPath.section]) {
        case kFacilities:
            selectedObject = facilities[indexPath.row];
            selectionSet = selectedFacilities;
            selectableSet = selectableFacilities;
            break;
        case kEquipment:
            selectedObject = equipment[indexPath.row];
            selectionSet = selectedEquipment;
            selectableSet = selectableEquipment;
            break;
        default:
            return;
    }
    
    //UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:indexPath];
    //[cell setSelected:TRUE animated:TRUE];
    //[cell setSelected:FALSE animated:TRUE];
    
    if([selectableSet containsObject:selectedObject]) {
        if([selectionSet containsObject:selectedObject])
            [selectionSet removeObject:selectedObject];
        else
            [selectionSet addObject:selectedObject];
        
        [self updateClearButtonAnimated:YES];
        [self updateSelectable];
        
        [self.tableView reloadData];
    } else {
        [tableView deselectRowAtIndexPath:indexPath animated:NO];
    }
}

@end
