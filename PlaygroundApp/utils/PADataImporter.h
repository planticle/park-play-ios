//
//  PADataImporter.h
//  PlaygroundApp
//
//  Created by James Watmuff on 27/05/13.
//  Copyright (c) 2013 Planticle. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PADataImporter : NSObject
+ (PADataImporter *)sharedInstance;
- (void)checkForUpdate;
- (void)updateDataWithJSONData:(NSDictionary *)data;

@end
