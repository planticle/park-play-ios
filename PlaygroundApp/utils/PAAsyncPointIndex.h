//
//  PAAsyncPointIndex.h
//  PlaygroundApp
//
//  Created by James Watmuff on 30/05/13.
//  Copyright (c) 2013 Planticle. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

@class PAAsyncPointIndex;

@protocol PAAsyncPointIndexDelegate
- (void)pointIndex:(PAAsyncPointIndex *)pointIndex hasUpdatedItemsToAdd:(NSMutableArray *)itemsToAdd andRemove:(NSMutableArray *)itemsToRemove;
@end

@interface PAAsyncPointIndex : NSObject
- (void)addCoordinate:(CLLocationCoordinate2D)coordinate withUserData:(id)userData;
- (void)updateWithExtentOfCoordinate:(CLLocationCoordinate2D)coordA coordinate:(CLLocationCoordinate2D)coordB coordinate:(CLLocationCoordinate2D)coordC coordinate:(CLLocationCoordinate2D)coordD;

@property (nonatomic, weak) id <PAAsyncPointIndexDelegate> delegate;

@end
