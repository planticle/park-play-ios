//
//  PAAsyncPointIndex.m
//  PlaygroundApp
//
//  Created by James Watmuff on 30/05/13.
//  Copyright (c) 2013 Planticle. All rights reserved.
//

#import "PAAsyncPointIndex.h"
#import "PAPointIndex.h"

@implementation PAAsyncPointIndex {
    PAPointIndex *pointIndex;
    dispatch_queue_t queue;
    CLLocationCoordinate2D extent[4];
    BOOL hasExtent;
}

@synthesize delegate;

- (id)init
{
    self = [super init];
    if(self) {
        pointIndex = [[PAPointIndex alloc] init];
        queue = dispatch_queue_create("com.planticle.goPlay.pointIndex", NULL);
    }
    return self;
}

- (void)addCoordinate:(CLLocationCoordinate2D)coordinate withUserData:(id)userData
{
    [pointIndex addCoordinate:coordinate withUserData:userData];
}

- (void)updateWithExtentOfCoordinate:(CLLocationCoordinate2D)coordA coordinate:(CLLocationCoordinate2D)coordB coordinate:
    (CLLocationCoordinate2D)coordC coordinate:(CLLocationCoordinate2D)coordD {
    
    CLLocationCoordinate2D newExtent[] = {coordA, coordB, coordC, coordD};
    [self setExtent:newExtent];
    
    dispatch_async(queue, ^{
        CLLocationCoordinate2D currentExtent[4];
        if([self getExtent:currentExtent]) {
            NSMutableArray *itemsToAdd = [[NSMutableArray alloc] init];
            NSMutableArray *itemsToRemove = [[NSMutableArray alloc] init];
            
            [pointIndex updateItemsToAdd:itemsToAdd andRemove:itemsToRemove forExtentOfCoordinate:currentExtent[0] coordinate:currentExtent[1] coordinate:currentExtent[2] coordinate:currentExtent[3]];
            
            // call delegate on main queue
            // this is intentionally synchronous, as we don't want to update the main thread any faster than it can respond
            dispatch_sync(dispatch_get_main_queue(), ^{
                if(delegate) {
                    [delegate pointIndex:self hasUpdatedItemsToAdd:itemsToAdd andRemove:itemsToRemove];
                }
            });
        }
    });
    
}

- (BOOL)getExtent:(CLLocationCoordinate2D [])anExtent {
    @synchronized(self) {
        if(hasExtent) {
            for(int i = 0; i < 4; i++) anExtent[i] = extent[i];
            hasExtent = NO;
            return YES;
        } else {
            return NO;
        }
    }
}

- (void)setExtent:(CLLocationCoordinate2D [])anExtent {
    @synchronized(self) {
        for(int i = 0; i < 4; i++) extent[i] = anExtent[i];
        hasExtent = YES;
    }
}

@end
