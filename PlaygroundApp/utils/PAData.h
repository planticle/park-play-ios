//
//  PAData.h
//  PlaygroundApp
//
//  Created by James Watmuff on 28/05/13.
//  Copyright (c) 2013 Planticle. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Playground.h"

@interface PAData : NSObject

@property (nonatomic, retain) NSSet *playgrounds;
@property (nonatomic, retain) NSSet *facilities;
@property (nonatomic, retain) NSSet *equipment;

+(PAData *)sharedInstance;
- (NSSet *)favouritePlaygrounds;
- (NSSet *)ourFavouritePlaygrounds;
- (NSSet *)playgroundsWithFacilities:(NSSet *)selectedFacilities andEquipment:(NSSet *)selectedEquipment;

- (void)setFavourite:(BOOL)favourite forPlayground:(Playground *)playground;
- (NSSet *)getFavouriteIds;
@end
