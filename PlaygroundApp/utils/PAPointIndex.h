//
//  PAPointIndex.h
//  PlaygroundApp
//
//  Created by James Watmuff on 21/05/13.
//  Copyright (c) 2013 Planticle. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

@interface PAPointIndex : NSObject
- (void)addCoordinate:(CLLocationCoordinate2D)coordinate withUserData:(id)userData;
- (void)updateItemsToAdd:(NSMutableArray *)itemsToAdd andRemove:(NSMutableArray *)itemsToRemove forExtentOfCoordinate:(CLLocationCoordinate2D)coordA coordinate:(CLLocationCoordinate2D)coordB coordinate:(CLLocationCoordinate2D)coordC coordinate:(CLLocationCoordinate2D)coordD;
@end
