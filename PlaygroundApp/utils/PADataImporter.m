//
//  PADataImporter.m
//  PlaygroundApp
//
//  Created by James Watmuff on 27/05/13.
//  Copyright (c) 2013 Planticle. All rights reserved.
//

#import "PADataImporter.h"
#import <AFNetworking/AFNetworking.h>
#import "Playground.h"
#import "PlaygroundEquipment.h"
#import "PlaygroundFacility.h"
#import "Facility.h"
#import "Equipment.h"
#import "Photo.h"
#import "PAData.h"
#import "PAAppDelegate.h"

static PADataImporter *sharedInstance = nil;

@implementation PADataImporter {
    BOOL updateInProgress;
}

+ (PADataImporter *)sharedInstance
{
    if(sharedInstance == nil) {
        sharedInstance = [[PADataImporter alloc] init];
    }
    return sharedInstance;
}

- (BOOL)haveCheckedRecently
{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSInteger lastCheckTimestamp = [userDefaults integerForKey:@"lastCheckTimestamp"];
    NSInteger nowTimestamp = [[NSDate date] timeIntervalSince1970];
    
    if(nowTimestamp - lastCheckTimestamp < 24 * 60 * 60) {
        return YES;
    } else {
        [userDefaults setInteger:nowTimestamp forKey:@"lastCheckTimestamp"];
        return NO;
    }
}

- (void)checkForUpdate
{
    //if([self haveCheckedRecently]) return;
    if(updateInProgress) return;
    updateInProgress = YES;
    
    @try {
        NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
        NSInteger localUpdateTimestamp = [userDefaults integerForKey:@"lastUpdateTimestamp"];
        
        NSURL *url = [NSURL URLWithString:@"https://s3-ap-southeast-2.amazonaws.com/goplay.planticle.com.au/update.json"];
        NSURLRequest *request = [NSURLRequest requestWithURL:url];
        AFJSONRequestOperation *operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:request success:^(NSURLRequest *request, NSHTTPURLResponse *response, id json) {
            NSDictionary *updateInfo = (NSDictionary *)json;
            NSInteger remoteUpdateTimestamp = [[updateInfo valueForKey:@"lastUpdateTimestamp"] integerValue];
            
            if(remoteUpdateTimestamp > localUpdateTimestamp) {
                [self updateDataWithTimestamp:remoteUpdateTimestamp];
            } else {
                NSLog(@"Data is up to date");
                updateInProgress = NO;
            }
        } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
            NSLog(@"Error getting update: %@", error);
            updateInProgress = NO;
        }];
        [operation start];
    } @catch(NSException *e) {
        updateInProgress = NO;
        @throw e;
    }
}

- (void)updateDataWithTimestamp:(NSInteger)remoteUpdateTimestamp
{
    @try {
        NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"https://s3-ap-southeast-2.amazonaws.com/goplay.planticle.com.au/data-%d.json", remoteUpdateTimestamp]];
        NSURLRequest *request = [NSURLRequest requestWithURL:url];
        
        
        PAAppDelegate *appDelegate = (PAAppDelegate *)[[UIApplication sharedApplication] delegate];
        NSURL *jsonURL = [[appDelegate applicationDocumentsDirectory] URLByAppendingPathComponent:@"data.json"];
        
        AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
        operation.outputStream = [NSOutputStream outputStreamWithURL:jsonURL append:NO];
        [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
            // set downloaded file as not for backup
            [jsonURL setResourceValue: [NSNumber numberWithBool: YES] forKey: NSURLIsExcludedFromBackupKey error:nil];
            
            NSLog(@"Downloaded update. Applying locally");
            NSInputStream *inputStream = [NSInputStream inputStreamWithURL:jsonURL];
            [inputStream open];
            id json = [NSJSONSerialization JSONObjectWithStream:inputStream options:nil error:nil];
            [self updateDataWithJSONData:(NSDictionary *)json];
            
            [[NSUserDefaults standardUserDefaults] setInteger:remoteUpdateTimestamp forKey:@"lastUpdateTimestamp"];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"DataWasUpdated" object:nil];

        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            NSLog(@"Error getting data: %@", error);
            updateInProgress = NO;
        }];
         
        [operation start];
    } @catch(NSException *e) {
        @throw e;
    } @finally {
        updateInProgress = NO;
    }
}

- (void)updateDataWithJSONData:(NSDictionary *)data
{
    NSLog(@"Begin data load: %f", [[NSDate date] timeIntervalSince1970]);
    NSMutableSet *paPlaygrounds = [[NSMutableSet alloc] init];
    NSMutableSet *paFacilities = [[NSMutableSet alloc] init];
    NSMutableSet *paEquipment = [[NSMutableSet alloc] init];
    
    NSSet *favouriteIds = [[PAData sharedInstance] getFavouriteIds];
    
    NSArray *playgrounds = [data objectForKey:@"Playgrounds"];
    NSArray *facilities = [data objectForKey:@"Facilities"];
    NSArray *equipment = [data objectForKey:@"Equipment"];
    
    NSMutableDictionary *facilityMap = [[NSMutableDictionary alloc] init];
    NSMutableDictionary *equipmentMap = [[NSMutableDictionary alloc] init];
    
    for(NSString *f in facilities) {
        Facility *facility = [self createFacility:f description:f];
        [paFacilities addObject:facility];
        [facilityMap setValue:facility forKey:f];
    }
    for(NSString *f in equipment) {
        Equipment *equipment = [self createEquipment:f description:f];
        [paEquipment addObject:equipment];
        [equipmentMap setValue: equipment forKey:f];
    }
    
    NSInteger dummyId = 1000000;
    for(NSDictionary *playgroundData in playgrounds) {
        NSString *name = [playgroundData objectForKey:@"Name"];
        NSString *access = [playgroundData objectForKey:@"Access Roads"];
        NSString *suburb = [playgroundData objectForKey:@"Suburb"];
        NSString *coords = [playgroundData objectForKey:@"Coordinates"];
        NSString *address = [NSString stringWithFormat:@"%@, %@ SA", access, suburb];
        
        Playground *playground = [self createPlayground:name address:address];
        if(coords) {
            NSArray *latlon = [coords componentsSeparatedByString:@","];
            NSString *latStr = [latlon objectAtIndex:0];
            NSString *lonStr = [latlon objectAtIndex:1];
            playground.lat = [NSNumber numberWithDouble:[latStr doubleValue]];
            playground.lon = [NSNumber numberWithDouble:[lonStr doubleValue]];
        } else {
            playground.lat = playground.lon = @0.0;
        }

        NSInteger playgroundId = [[playgroundData objectForKey:@"id"] integerValue];
        if(playgroundId == 0) playgroundId = dummyId++;
        playground.playgroundId = [NSNumber numberWithInteger:playgroundId];
        
        playground.favourite = [NSNumber numberWithBool:[favouriteIds containsObject:[NSNumber numberWithInteger:playgroundId]]];
        playground.ourFavourite = [NSNumber numberWithBool:[[playgroundData objectForKey:@"OurFavourite"] boolValue]];
        
        NSMutableArray *items = [[NSMutableArray alloc] init];
        NSMutableArray *counts = [[NSMutableArray alloc] init];
        
        for(NSString *f in facilities) {
            id countData = [playgroundData objectForKey:f];
            NSNumber *count = @0;
            if([countData isKindOfClass:[NSNumber class]]) {
                count = (NSNumber *)countData;
            } else if([countData isKindOfClass:[NSString class]]) {
                count = [self countFromString:countData];
            }
            
            if(![count isEqualToNumber:@0]) {
                [items addObject:[facilityMap objectForKey:f]];
                [counts addObject:count];
            }
        }
        
        [self addFacilities:items toPlayground:playground withCounts:counts];
        
        [items removeAllObjects];
        [counts removeAllObjects];
        
        for(NSString *e in equipment) {
            id countData = [playgroundData objectForKey:e];
            NSNumber *count = @0;
            if([countData isKindOfClass:[NSNumber class]]) {
                count = (NSNumber *)countData;
            } else if([countData isKindOfClass:[NSString class]]) {
                count = [self countFromString:countData];
            }
            
            if(![count isEqualToNumber:@0]) {
                [items addObject:[equipmentMap objectForKey:e]];
                [counts addObject:count];
            }
        }
        [self addEquipment:items toPlayground:playground withCounts:counts];
        
        // Add images
        NSArray *playgroundImages = [playgroundData objectForKey:@"Photos"];
        if(playgroundImages != nil) {
            for(NSDictionary *imageInfo in playgroundImages) {                
                Photo *photo = [[Photo alloc] init];
                photo.name = [imageInfo objectForKey:@"name"];
                photo.width = [NSNumber numberWithInt:[((NSString *)[imageInfo objectForKey:@"width"]) intValue]];
                photo.height = [NSNumber numberWithInt:[((NSString *)[imageInfo objectForKey:@"height"]) intValue]];
                photo.playground = playground;
                [playground.photos addObject:photo];
            }
        
        }
        [paPlaygrounds addObject:playground];
    }
    
    // ---- Ideally the following should be synchronized to prevent inconsistent data
    PAData *paData = [PAData sharedInstance];
    paData.playgrounds = paPlaygrounds;
    paData.facilities = paFacilities;
    paData.equipment = paEquipment;
    
    // ---- End
    
    updateInProgress = NO;
    NSLog(@"Completed data load: %f", [[NSDate date] timeIntervalSince1970]);
}

-(NSNumber *)countFromString:(NSString *)str
{
    if([str isEqualToString:@"true"]) return @-1;
    if([str isEqualToString:@"false"]) return @0;
    return [[NSNumber alloc] initWithInt:[str intValue]];
}


-(void)addFacilities:(NSArray *)facilities toPlayground:(Playground *)playground withCounts:(NSArray *)counts
{
    int i = 0;
    for(Facility *facility in facilities) {
        PlaygroundFacility *pf = [[PlaygroundFacility alloc] init];
        pf.facility = facility;
        pf.playground = playground;
        pf.count = [counts objectAtIndex:i++];
        [playground.facilities addObject:pf];
    }
}

-(void)addEquipment:(NSArray *)equipment toPlayground:(Playground *)playground withCounts:(NSArray *)counts
{
    int i = 0;
    for(Equipment *equip in equipment) {
        PlaygroundEquipment *pe = [[PlaygroundEquipment alloc] init];
        pe.equipment = equip;
        pe.playground = playground;
        pe.count = [counts objectAtIndex:i++];
        [playground.equipment addObject:pe];
    }
}

-(Facility *)createFacility:(NSString *)name description:(NSString *)description
{
    Facility *facility = [[Facility alloc] init];
    
    facility.name = [[name lowercaseString] stringByReplacingOccurrencesOfString:@"/" withString:@" "];
    facility.desc = description;
    return facility;
}

-(Equipment *)createEquipment:(NSString *)name description:(NSString *)description
{
    Equipment *equip = [[Equipment alloc] init];
    equip.name = [[name lowercaseString] stringByReplacingOccurrencesOfString:@"/" withString:@" "];
    equip.desc = description;
    return equip;
}

-(Playground *)createPlayground:(NSString *)name address:(NSString *)address
{
    Playground *playground = [[Playground alloc] init];
    playground.name = name;
    playground.address = address;
    playground.equipment = [[NSMutableSet alloc] init];
    playground.facilities = [[NSMutableSet alloc] init];
    playground.photos = [[NSMutableSet alloc] init];
    
    return playground;
}

@end
