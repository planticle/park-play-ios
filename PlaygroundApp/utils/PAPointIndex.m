//
//  PAPointIndex.m
//  PlaygroundApp
//
//  Created by James Watmuff on 21/05/13.
//  Copyright (c) 2013 Planticle. All rights reserved.
//

#import "PAPointIndex.h"
#import "PAUtil.h"

@implementation PAPointIndex {
    NSMutableDictionary *pointMap;
    
    int lastMinX, lastMinY, lastMaxX, lastMaxY, lastScale;
    BOOL firstUpdate;
}

- (id)init
{
    self = [super init];
    
    pointMap = [[NSMutableDictionary alloc] init];
    firstUpdate = true;
    
    return self;
}

- (void)addCoordinate:(CLLocationCoordinate2D)coordinate withUserData:(id)userData
{
    for(int scale = 0; scale < 30; scale++) {
        NSString *key = [self keyForScale:scale lat:coordinate.latitude lon:coordinate.longitude];
        if(pointMap[key]) {
            break;
        } else {
            pointMap[key] = userData;
        }
    }
}


- (void)updateItemsToAdd:(NSMutableArray *)itemsToAdd andRemove:(NSMutableArray *)itemsToRemove forExtentOfCoordinate:(CLLocationCoordinate2D)coordA coordinate:(CLLocationCoordinate2D)coordB coordinate:(CLLocationCoordinate2D)coordC coordinate:(CLLocationCoordinate2D)coordD
{
    CLLocationDegrees minLat = MIN(MIN(MIN(coordA.latitude, coordB.latitude), coordC.latitude), coordD.latitude);
    CLLocationDegrees maxLat = MAX(MAX(MAX(coordA.latitude, coordB.latitude), coordC.latitude), coordD.latitude);
    CLLocationDegrees minLon = MIN(MIN(MIN(coordA.longitude, coordB.longitude), coordC.longitude), coordD.longitude);
    CLLocationDegrees maxLon = MAX(MAX(MAX(coordA.longitude, coordB.longitude), coordC.longitude), coordD.longitude);
    
    CLLocationDegrees latExtent = maxLat - minLat;
    CLLocationDegrees lonExtent = maxLon - minLon;
    
    CLLocationDegrees maxExtent = MAX(latExtent, lonExtent);

    // More cores -> show more markers
    double scaleAdjustment = [PAUtil numberOfCores] > 1 ? 0 : 1.5;
    int scale = MIN(MAX(log2(maxExtent * 1000) - 3.5 + scaleAdjustment, 0), 29);

    int minX, maxX, minY, maxY;
    
    [self populateX:&minX y:&minY forLat:minLat lon:minLon scale:scale];
    [self populateX:&maxX y:&maxY forLat:maxLat lon:maxLon scale:scale];
    
    int buffer = [PAUtil numberOfCores] > 1 ? 2 : 1;
    minX-=buffer;
    maxX+=buffer;
    minY-=buffer;
    maxY+=buffer;
    
    if(scale == lastScale && minX == lastMinX && minY == lastMinY && maxX == lastMaxX && maxY == lastMaxY) return;
    
    if(!firstUpdate) {
        for(int x = lastMinX; x <= lastMaxX; x++) {
            for(int y = lastMinY; y <= lastMaxY; y++) {
                if(scale == lastScale && x <= maxX && x >= minX && y <= maxY && y >= minY) continue;
                id item = pointMap[[self keyForScale:lastScale x:x y:y]];
                if(item) [itemsToRemove addObject:item];
            }
        }
    }
    
    for(int x = minX; x <= maxX; x++) {
        for(int y = minY; y <= maxY; y++) {
            if(scale == lastScale && !firstUpdate && x <= lastMaxX && x >= lastMinX && y <= lastMaxY && y >= lastMinY) continue;
            id item = pointMap[[self keyForScale:scale x:x y:y]];
            if(item) [itemsToAdd addObject:item];
        }
    }
    
    lastMinX = minX;
    lastMaxX = maxX;
    lastMinY = minY;
    lastMaxY = maxY;
    lastScale = scale;
    firstUpdate = NO;
    
    //NSLog(@"Items to add: %d, items to remove: %d", itemsToAdd.count, itemsToRemove.count);
}

- (void)populateX:(int *)x y:(int *)y forLat:(CLLocationDegrees)lat lon:(CLLocationDegrees)lon scale:(int)scale
{
    *x = (int)(lon * 1000) >> scale;
    *y = (int)(lat * 1000) >> scale;
}

- (NSString *)keyForScale:(int)scale lat:(CLLocationDegrees)lat lon:(CLLocationDegrees)lon
{
    int x, y;
    [self populateX:&x y:&y forLat:lat lon:lon scale:scale];
    return [self keyForScale:scale x:x y:y];
}

- (NSString *)keyForScale:(int)scale x:(int)x y:(int)y
{
    return [NSString stringWithFormat:@"%d_%d_%d", scale, x, y];
}

@end
