//
//  PAData.m
//  PlaygroundApp
//
//  Created by James Watmuff on 28/05/13.
//  Copyright (c) 2013 Planticle. All rights reserved.
//

#import "PAData.h"
#import "Playground.h"

static PAData *sharedInstance = nil;

@implementation PAData

@synthesize playgrounds;
@synthesize facilities;
@synthesize equipment;

+(PAData *)sharedInstance {
    if(sharedInstance == nil) {
        sharedInstance = [[PAData alloc] init];
    }
    return sharedInstance;
}

- (NSSet *)favouritePlaygrounds
{
    NSMutableSet *result = [[NSMutableSet alloc] initWithCapacity:playgrounds.count];
    for(Playground *playground in playgrounds) {
        if(playground.favourite.boolValue) {
            [result addObject:playground];
        }
    }
    return result;
}

- (NSSet *)ourFavouritePlaygrounds
{
    NSMutableSet *result = [[NSMutableSet alloc] initWithCapacity:playgrounds.count];
    for(Playground *playground in playgrounds) {
        if(playground.ourFavourite.boolValue) {
            [result addObject:playground];
        }
    }
    return result;
}

- (NSSet *)playgroundsWithFacilities:(NSSet *)selectedFacilities andEquipment:(NSSet *)selectedEquipment
{
    NSMutableSet *result = [[NSMutableSet alloc] initWithCapacity:playgrounds.count];
    
    for(Playground *playground in playgrounds) {
        BOOL matches = selectedEquipment.count == 0 || [selectedEquipment isSubsetOfSet:playground.allEquipment];
        matches = matches && (selectedFacilities.count == 0 || [selectedFacilities isSubsetOfSet:playground.allFacilities]);
        if(matches) {
            [result addObject:playground];
        }
    }
    return result;
}

- (void)setFavourite:(BOOL)favourite forPlayground:(Playground *)playground
{
    NSMutableSet *ids = [[NSMutableSet alloc] initWithSet:[self getFavouriteIds]];
    if(favourite) {
        [ids addObject:playground.playgroundId];
    } else {
        [ids removeObject:playground.playgroundId];
    }
    [self saveFavouriteIds:ids];
    playground.favourite = [NSNumber numberWithBool:favourite];
}

-(NSSet *)getFavouriteIds
{
    NSArray *ids = [[NSUserDefaults standardUserDefaults] objectForKey:@"favouriteIds"];
    if(ids == nil) ids = @[];
//    NSLog(@"Loaded ids: %@", ids);
    return [NSSet setWithArray:ids];
}

-(void)saveFavouriteIds:(NSSet *)ids
{
//    NSLog(@"Saving ids: %@", ids);
    [[NSUserDefaults standardUserDefaults] setObject:[ids allObjects] forKey:@"favouriteIds"];
}

@end
