//
//  PASortViewController.h
//  PlaygroundApp
//
//  Created by James Watmuff on 5/03/13.
//  Copyright (c) 2013 Planticle. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <GoogleMaps/GoogleMaps.h>

@class PASortViewController;

@protocol PASortViewControllerDelegate
- (void)sortViewControllerDidFinish:(PASortViewController *)controller;
@end

@interface PASortViewController : UITableViewController  <GMSMapViewDelegate>

@property (nonatomic) CLLocationCoordinate2D pin;
@property (nonatomic) NSInteger sortMode;
@property (nonatomic, weak) id <PASortViewControllerDelegate> delegate;

@end
