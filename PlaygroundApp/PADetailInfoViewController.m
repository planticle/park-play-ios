//
//  PADetailFacilitiesViewController.m
//  PlaygroundApp
//
//  Created by James Watmuff on 4/03/13.
//  Copyright (c) 2013 Planticle. All rights reserved.
//

#import "PADetailInfoViewController.h"
#import "Playground.h"
#import "Facility.h"
#import "PlaygroundFacility.h"
#import "Equipment.h"
#import "PlaygroundEquipment.h"
#import "PADetailPhotosViewController.h"
#import "PAData.h"
#import "PAUtil.h"
#import <DTCoreText.h>

#import <QuartzCore/QuartzCore.h>

typedef NS_ENUM(NSUInteger, SectionType) {
    kDetails,
    kFacilities,
    kEquipment,
    kFavourite
};

@interface PADetailInfoViewController () {
    NSArray *facilities;
    NSArray *equipment;
    DTAttributedLabel *titleLabel;
    UIView *titleView;
}

@end

@implementation PADetailInfoViewController

@synthesize playground;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }

    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    UIBarButtonItem *photosButton = [PAUtil barButtonItemWithImage:[UIImage imageNamed:@"images/camera"]
                                                            target:self
                                                            action:@selector(didPressPhotosButton)];
    
    UIBarButtonItem *backButton = [PAUtil backButtonWithTarget:self action:@selector(didPressBackButton)];
    
    self.title = @"Details";
    self.navigationItem.rightBarButtonItem = photosButton;
    self.navigationItem.leftBarButtonItem = backButton;

    self.tableView.backgroundView = nil;
    self.tableView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"images/bg"]];
    
    if([[[UIDevice currentDevice] systemVersion] doubleValue] >= 7) {
        self.tableView.separatorColor = [UIColor clearColor];
    }

    [self loadFacilities];
    [self loadEquipment];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    // Reset navigation bar to default
    [self updateNavigationBarColor:animated];
}

- (void)updateNavigationBarColor:(BOOL)animated
{
    if(animated) {
        [UIView animateWithDuration:0.4f animations:^(void) {
            [self updateNavigationBarColor:NO];
        }];
        return;
    }
    self.navigationController.navigationBar.backgroundColor = [[UINavigationBar appearance] backgroundColor];
    [self.navigationController.navigationBar setBackgroundImage:[[UINavigationBar appearance] backgroundImageForBarMetrics:UIBarMetricsDefault] forBarMetrics:UIBarMetricsDefault];
}


- (void)didPressBackButton
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didPressPhotosButton
{
    PADetailPhotosViewController *viewController = [[PADetailPhotosViewController alloc] init];
    viewController.playground = playground;
    
    UIBarButtonItem *detailsButton = [PAUtil barButtonItemWithTitle:@"Done"
                                                             target:self
                                                             action:@selector(didPressDetailsButton)];
    viewController.navigationItem.rightBarButtonItem = detailsButton;
    
    UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:viewController];
    
    // iOS 7 Adjustments
    if([[[UIDevice currentDevice] systemVersion] doubleValue] < 7) {
        navController.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
    }
    navController.navigationBar.tintColor = self.navigationController.navigationBar.tintColor;
    [self presentViewController:navController animated:YES completion:nil];
}

- (void)didPressDetailsButton
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)loadFacilities
{
    NSSortDescriptor *descriptor = [[NSSortDescriptor alloc] initWithKey:@"facility.name" ascending:YES];
    facilities =  [playground.facilities sortedArrayUsingDescriptors:@[descriptor]];
}

- (void)loadEquipment
{
    NSSortDescriptor *descriptor = [[NSSortDescriptor alloc] initWithKey:@"equipment.name" ascending:YES];
    equipment =  [playground.equipment sortedArrayUsingDescriptors:@[descriptor]];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (SectionType) typeOfSection:(NSInteger)section
{
    if(section > 0 && facilities.count == 0) section++;
    if(section > 1 && equipment.count == 0) section++;
    
    switch(section) {
        case 0:
            return kDetails;
        case 1:
            return kFacilities;
        case 2:
            return kEquipment;
        case 3:
            return kFavourite;
        default:
            NSLog(@"Invalid section: %d", section);
            return -1;
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 2 + (facilities.count > 0 ? 1 : 0) + (equipment.count > 0 ? 1 : 0);
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    switch([self typeOfSection:section]) {
        case kDetails: return 1;
        case kFacilities: return facilities.count;
        case kEquipment: return equipment.count;
        case kFavourite: return 1;
        default: return 0;
    }
}

- (NSData *)getTitleHtml
{
    return [[NSString stringWithFormat:@"<h1>%@</h1><br/>%@", playground.name, playground.address] dataUsingEncoding:NSUTF8StringEncoding];
}

- (DTAttributedLabel *)getTitleLabel
{
    if(!titleLabel) {
        titleLabel = [[DTAttributedLabel alloc] initWithFrame:CGRectZero];
        titleLabel.attributedString = [[NSAttributedString alloc] initWithHTMLData:[self getTitleHtml]
                                                                           options:nil
                                                                documentAttributes:nil];
        titleLabel.layoutFrameHeightIsConstrainedByBounds = NO;
    }
    return titleLabel;
}

- (void) styleButton:(UIButton *)button
{
    button.titleLabel.font = [UIFont fontWithName:@"American Typewriter" size:18.0];
    button.titleLabel.textColor = [UIColor whiteColor];
    [button setBackgroundImage:[PAUtil imageWithColor:[PAUtil darkGreen]] forState:UIControlStateNormal];
    [button setBackgroundImage:[PAUtil imageWithColor:[PAUtil lightGreen]] forState:UIControlStateHighlighted];
}

- (UIView *)getTitleView
{
    if(!titleView) {
        UILabel *name = [[UILabel alloc] initWithFrame:CGRectMake(0,0,300.f,CGFLOAT_MAX)];
        name.text = playground.name;
        name.font = [UIFont boldSystemFontOfSize:24.f];
        name.numberOfLines = 0;
        name.backgroundColor = [UIColor clearColor];
        [name sizeToFit];
        
        UILabel *address = [[UILabel alloc] initWithFrame:CGRectMake(0,0,300.f,CGFLOAT_MAX)];
        address.text = playground.address;
        address.numberOfLines = 0;
        address.backgroundColor = [UIColor clearColor];
        [address sizeToFit];
        
        UIButton *directionsButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 145, 44)];
        [self styleButton:directionsButton];
        [directionsButton setTitle:@"Get Directions" forState:UIControlStateNormal];
        [directionsButton addTarget:self action:@selector(getDirections) forControlEvents:UIControlEventTouchUpInside];
        
        UIButton *mapButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 145, 44)];
        [self styleButton:mapButton];
        [mapButton setTitle:@"Show on Map" forState:UIControlStateNormal];
        [mapButton addTarget:self action:@selector(showOnMap) forControlEvents:UIControlEventTouchUpInside];

        name.frame = CGRectOffset(name.frame, 10, 5);
        address.frame = CGRectOffset(address.frame, 10, name.bounds.size.height + 10);
        directionsButton.frame = CGRectOffset(directionsButton.frame, 10, name.bounds.size.height + address.bounds.size.height + 20);
        mapButton.frame = CGRectOffset(mapButton.frame, 165, name.bounds.size.height + address.bounds.size.height + 20);
        
        titleView = [[UIView alloc] initWithFrame:CGRectMake(0,0,320,name.bounds.size.height + address.bounds.size.height+20+44)];
        [titleView addSubview:name];
        [titleView addSubview:address];
        [titleView addSubview:directionsButton];
        [titleView addSubview:mapButton];
        titleView.backgroundColor = [UIColor clearColor];
    }
    return titleView;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell;
    SectionType sectionType = [self typeOfSection:indexPath.section];
    UISwitch *favouriteSwitch;

    if(sectionType == kFacilities || sectionType == kEquipment) {
        cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if(cell == nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier];
            if([[[UIDevice currentDevice] systemVersion] doubleValue] >= 7) {
                CGRect borderFrame = CGRectMake(0, cell.bounds.size.height - 0.5f, cell.bounds.size.width, 0.5f);
                borderFrame = UIEdgeInsetsInsetRect(borderFrame, cell.separatorInset);
                UIView *border = [[UIView alloc] initWithFrame:borderFrame];
                border.backgroundColor = [UIColor colorWithWhite:0 alpha:0.1f];
                border.tag = 10;
                [cell.contentView addSubview:border];
            }
        }
    }

    if([[[UIDevice currentDevice] systemVersion] doubleValue] >= 7) {
        [cell.contentView viewWithTag:10].hidden = (indexPath.row == [self tableView:tableView numberOfRowsInSection:indexPath.section] - 1);
    }

    if(sectionType == kDetails) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
        cell.backgroundView = [UIView new];
        cell.backgroundColor = [UIColor clearColor];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        [cell addSubview:[self getTitleView]];

    } else if(sectionType == kFacilities) {
        PlaygroundFacility *pf = facilities[indexPath.row];
        cell.textLabel.text = pf.facility.desc;
        cell.selectionStyle = UITableViewCellSelectionStyleNone;

        if(![pf.count isEqualToNumber:@0]) {
            cell.detailTextLabel.text = [pf.count isEqualToNumber:@-1] ? @"" : [pf.count stringValue];
            cell.accessoryType = [pf.count isEqualToNumber:@-1] ? UITableViewCellAccessoryCheckmark : UITableViewCellAccessoryNone;
        } else {
            cell.detailTextLabel.text = @"";
            cell.accessoryType = UITableViewCellAccessoryNone;
        }
    
        NSString *resource = [NSString stringWithFormat:@"images/icons/%@", pf.facility.name];
        UIImage *theImage = [UIImage imageNamed:resource];
        cell.imageView.image = theImage;

    } else if(sectionType == kEquipment) {
        PlaygroundEquipment *pe = equipment[indexPath.row];
        cell.textLabel.text = pe.equipment.desc;
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        if(![pe.count isEqualToNumber:@0]) {
            cell.detailTextLabel.text = [pe.count isEqualToNumber:@-1] ? @"" : [pe.count stringValue];
            cell.accessoryType = [pe.count isEqualToNumber:@-1] ? UITableViewCellAccessoryCheckmark : UITableViewCellAccessoryNone;
        } else {
            cell.detailTextLabel.text = @"";
            cell.accessoryType = UITableViewCellAccessoryNone;
        }
        
        NSString *resource = [NSString stringWithFormat:@"images/icons/%@", pe.equipment.name];
        UIImage *theImage = [UIImage imageNamed:resource];
        cell.imageView.image = theImage;
    } else if(sectionType == kFavourite) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:nil];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.textLabel.text = @"Add to Favourites";
        favouriteSwitch = [[UISwitch alloc] initWithFrame:CGRectZero];
        favouriteSwitch.on = playground.favourite.boolValue;
        favouriteSwitch.onTintColor = [PAUtil orange];
        [favouriteSwitch addTarget:self action:@selector(didToggleFavouriteSwitch:) forControlEvents:UIControlEventValueChanged];
        cell.accessoryView = favouriteSwitch;
        cell.imageView.image = [UIImage imageNamed:@"images/icons/favourite"];
    }
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if([self typeOfSection:indexPath.section] == kDetails && indexPath.row == 0) {
        return [self getTitleView].bounds.size.height;
    } else {
        return tableView.rowHeight;
    }
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    switch([self typeOfSection:section]) {
        case kFacilities: return @"Facilties";
        case kEquipment: return @"Equipment";
        default: return @"";
    }
}

-(NSString *)tableView:(UITableView *)tableView titleForFooterInSection:(NSInteger)section
{
    if(section == [self numberOfSectionsInTableView:tableView] - 1) {
        return @"© 2013 Planticle";
    } else {
        return nil;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    // iOS 7 Adjustments
    if([[[UIDevice currentDevice] systemVersion] doubleValue] >= 7) {
        return (section == 0) ? 1.f : tableView.sectionHeaderHeight;
    }
    return (section == 0) ? 0 : 30.f;
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
}

#pragma mark - Action sheet delegate

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(buttonIndex == 0) {
        [self getDirections];
    } else if(buttonIndex == 1) {
        [self showOnMap];
    }
}

- (void)actionSheet:(UIActionSheet *)actionSheet willDismissWithButtonIndex:(NSInteger)buttonIndex
{
    [self.tableView deselectRowAtIndexPath:[self.tableView indexPathForSelectedRow] animated:YES];
}

# pragma mark - Custom methods

- (void)getDirections
{
    NSString *coords = [NSString stringWithFormat:@"%@,%@", playground.lat, playground.lon];
    [self directToAddress:coords];
}

- (void)showOnMap
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"ShowPlaygroundOnMap" object:playground];
}

- (void)directToAddress:(NSString *)address
{
    NSString *url;
    if([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"comgooglemaps://"]]) {
        url = [NSString stringWithFormat:@"comgooglemaps://?daddr=%@", address];
    } else {
        url = [NSString stringWithFormat:@"http://maps.apple.com://?daddr=%@", address];
    }
    url = [url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSLog(@"opening url: %@", url);
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:url]];
}

- (void)didToggleFavouriteSwitch:(UISwitch *)favouriteSwitch
{
    [[PAData sharedInstance] setFavourite:favouriteSwitch.on forPlayground:playground];
}

@end
