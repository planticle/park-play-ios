//
//  Equipment.h
//  PlaygroundApp
//
//  Created by James Watmuff on 6/04/13.
//  Copyright (c) 2013 Planticle. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class PlaygroundEquipment;

@interface Equipment : NSObject

@property (nonatomic, retain) NSString * desc;
@property (nonatomic, retain) NSString * name;
//@property (nonatomic, retain) NSSet *playgrounds;

@end
