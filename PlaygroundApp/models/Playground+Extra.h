//
//  Playground+Extra.h
//  PlaygroundApp
//
//  Created by James Watmuff on 6/03/13.
//  Copyright (c) 2013 Planticle. All rights reserved.
//

#import "Playground.h"
#import <CoreLocation/CoreLocation.h>

@interface Playground (Extra)

@property (readonly, nonatomic, strong) CLLocation *location;
@property (readonly, nonatomic) CLLocationCoordinate2D coordinate;

@end
