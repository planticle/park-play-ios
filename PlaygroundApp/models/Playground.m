//
//  Playground.m
//  PlaygroundApp
//
//  Created by James Watmuff on 28/05/13.
//  Copyright (c) 2013 Planticle. All rights reserved.
//

#import "Playground.h"
#import "Photo.h"
#import "PlaygroundEquipment.h"
#import "PlaygroundFacility.h"


@implementation Playground

@synthesize address;
@synthesize desc;
@synthesize favourite;
@synthesize ourFavourite;
@synthesize lat;
@synthesize lon;
@synthesize name;
@synthesize playgroundId;
@synthesize equipment;
@synthesize facilities;
@synthesize photos;

- (NSSet *)allFacilities
{
    NSMutableSet *set = [[NSMutableSet alloc] initWithCapacity:facilities.count];
    for(PlaygroundFacility *pf in facilities) {
        [set addObject:pf.facility];
    }
    return set;
}

- (NSSet *)allEquipment
{
    NSMutableSet *set = [[NSMutableSet alloc] initWithCapacity:equipment.count];
    for(PlaygroundEquipment *pe in equipment) {
        [set addObject:pe.equipment];
    }
    return set;
}

@end
