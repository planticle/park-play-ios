//
//  PlaygroundEquipment.h
//  PlaygroundApp
//
//  Created by James Watmuff on 6/04/13.
//  Copyright (c) 2013 Planticle. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Equipment, Playground;

@interface PlaygroundEquipment : NSObject

@property (nonatomic, retain) NSNumber * count;
@property (nonatomic, retain) Equipment *equipment;
@property (nonatomic, retain) Playground *playground;

@end
