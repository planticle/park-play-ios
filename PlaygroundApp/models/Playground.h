//
//  Playground.h
//  PlaygroundApp
//
//  Created by James Watmuff on 28/05/13.
//  Copyright (c) 2013 Planticle. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Photo, PlaygroundEquipment, PlaygroundFacility;

@interface Playground : NSObject

@property (nonatomic, retain) NSString * address;
@property (nonatomic, retain) NSString * desc;
@property (nonatomic, retain) NSNumber * favourite;
@property (nonatomic, retain) NSNumber * ourFavourite;
@property (nonatomic, retain) NSNumber * lat;
@property (nonatomic, retain) NSNumber * lon;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSNumber * playgroundId;
@property (nonatomic, retain) NSMutableSet *equipment;
@property (nonatomic, retain) NSMutableSet *facilities;
@property (nonatomic, retain) NSMutableSet *photos;

- (NSSet *)allFacilities;
- (NSSet *)allEquipment;

@end
