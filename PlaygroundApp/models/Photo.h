//
//  Photo.h
//  PlaygroundApp
//
//  Created by James Watmuff on 7/05/13.
//  Copyright (c) 2013 Planticle. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Playground;

@interface Photo : NSObject

@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSNumber * width;
@property (nonatomic, retain) NSNumber * height;
@property (nonatomic, retain) Playground *playground;

@end
