//
//  Playground+Extra.m
//  PlaygroundApp
//
//  Created by James Watmuff on 6/03/13.
//  Copyright (c) 2013 Planticle. All rights reserved.
//

#import "Playground+Extra.h"

@implementation Playground (Extra)

@dynamic location;
@dynamic coordinate;

- (CLLocation *)location {
    return [[CLLocation alloc] initWithLatitude:self.lat.doubleValue longitude:self.lon.doubleValue];
}
- (CLLocationCoordinate2D)coordinate {
    return CLLocationCoordinate2DMake(self.lat.doubleValue, self.lon.doubleValue);
}
@end
