//
//  Photo.m
//  PlaygroundApp
//
//  Created by James Watmuff on 7/05/13.
//  Copyright (c) 2013 Planticle. All rights reserved.
//

#import "Photo.h"
#import "Playground.h"


@implementation Photo

@synthesize name;
@synthesize width;
@synthesize height;
@synthesize playground;

@end
