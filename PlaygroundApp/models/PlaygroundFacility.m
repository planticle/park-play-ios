//
//  PlaygroundFacility.m
//  PlaygroundApp
//
//  Created by James Watmuff on 6/04/13.
//  Copyright (c) 2013 Planticle. All rights reserved.
//

#import "PlaygroundFacility.h"
#import "Facility.h"
#import "Playground.h"


@implementation PlaygroundFacility

@synthesize count;
@synthesize facility;
@synthesize playground;

@end
