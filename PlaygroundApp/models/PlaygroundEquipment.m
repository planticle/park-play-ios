//
//  PlaygroundEquipment.m
//  PlaygroundApp
//
//  Created by James Watmuff on 6/04/13.
//  Copyright (c) 2013 Planticle. All rights reserved.
//

#import "PlaygroundEquipment.h"
#import "Equipment.h"
#import "Playground.h"


@implementation PlaygroundEquipment

@synthesize count;
@synthesize equipment;
@synthesize playground;

@end
