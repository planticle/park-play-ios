//
//  Facility.m
//  PlaygroundApp
//
//  Created by James Watmuff on 6/04/13.
//  Copyright (c) 2013 Planticle. All rights reserved.
//

#import "Facility.h"
#import "PlaygroundFacility.h"


@implementation Facility

@synthesize desc;
@synthesize name;
//@synthesize playgrounds;

@end
