//
//  Equipment.m
//  PlaygroundApp
//
//  Created by James Watmuff on 6/04/13.
//  Copyright (c) 2013 Planticle. All rights reserved.
//

#import "Equipment.h"
#import "PlaygroundEquipment.h"


@implementation Equipment

@synthesize desc;
@synthesize name;
//@synthesize playgrounds;

@end
