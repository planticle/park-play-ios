//
//  PlaygroundFacility.h
//  PlaygroundApp
//
//  Created by James Watmuff on 6/04/13.
//  Copyright (c) 2013 Planticle. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Facility, Playground;

@interface PlaygroundFacility : NSObject

@property (nonatomic, retain) NSNumber * count;
@property (nonatomic, retain) Facility *facility;
@property (nonatomic, retain) Playground *playground;

@end
