//
//  PASortViewController.m
//  PlaygroundApp
//
//  Created by James Watmuff on 5/03/13.
//  Copyright (c) 2013 Planticle. All rights reserved.
//

#import "PASortViewController.h"
#import "PAUtil.h"
#import <GoogleMaps/GoogleMaps.h>

#define DISTANCE_FROM_ME 0
#define DISTANCE_FROM_PIN 1
#define ALPHABETICALLY 2
#define POPULARITY 3

@interface PASortViewController () {
    NSInteger selectedIndex;
    GMSMapView *mapView;
    GMSMapView *fullMapView;
}

@end

@implementation PASortViewController

@synthesize pin;
@synthesize sortMode;
@synthesize delegate;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {

    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    UIBarButtonItem *rightButton = [PAUtil barButtonItemWithTitle:@"Done" target:self action:@selector(didPressDoneButton)];
    self.navigationItem.rightBarButtonItem = rightButton;
    self.title = @"Sort";

    selectedIndex = sortMode;
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    if(mapView != nil) {
        [mapView animateToLocation:pin];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Custom methods


- (GMSMapView *)getMapView
{
    if(mapView == nil) {
        // Do any additional setup after loading the view.
        GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:pin.latitude
                                                                longitude:pin.longitude
                                                                     zoom:10];

        mapView = [GMSMapView mapWithFrame:CGRectMake(20,10,280,180) camera:camera];
        mapView.autoresizingMask = UIViewAutoresizingFlexibleWidth;
        [mapView setUserInteractionEnabled:NO];
        
        [self updatePinLocationOnMap:mapView];
    }
    mapView.frame = CGRectMake(20,10,280,180);
    return mapView;
}

- (GMSMapView *)getFullMapView
{
    if(fullMapView != nil) return fullMapView;
    
	// Do any additional setup after loading the view.
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:pin.latitude
                                                            longitude:pin.longitude
                                                                 zoom:10];
    fullMapView = [GMSMapView mapWithFrame:CGRectZero camera:camera];
    [fullMapView setUserInteractionEnabled:YES];
    fullMapView.myLocationEnabled = YES;
    fullMapView.delegate = self;
    
    //[self updatePinLocationOnMap:fullMapView];
    
    return fullMapView;
}

- (void)didPressDoneButton
{
    [self.delegate sortViewControllerDidFinish:self];
}

- (void)didPressBackButton
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)updatePinLocation:(CLLocationCoordinate2D)coordinate
{
    pin = coordinate;
    if(mapView != nil)
        [self updatePinLocationOnMap:mapView];
    if(fullMapView != nil)
        [self updatePinLocationOnMap:fullMapView];
}

- (void)updatePinLocationOnMap:(GMSMapView *)map
{
    [map clear];
    GMSMarker *marker = [GMSMarker markerWithPosition:pin];
    marker.icon = [UIImage imageNamed:@"images/pin_drop"];
    marker.animated = YES;
    marker.map = map;
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return selectedIndex == DISTANCE_FROM_PIN ? 2 : 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    switch(section) {
        case 0: return 3;
        case 1: return 1;
        default: return 0;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
//    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
    //[tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
 
    if(indexPath.section == 0) {
        switch(indexPath.row) {
            case 0:
                cell.textLabel.text = @"Distance from me";
                break;
            case 1:
                cell.textLabel.text = @"Distance from pin";
                break;
            case 2:
                cell.textLabel.text = @"Alphabetical";
                break;
            case 3:
                cell.textLabel.text = @"Popularity";
        }
        cell.accessoryType = indexPath.row == selectedIndex ? UITableViewCellAccessoryCheckmark : UITableViewCellAccessoryNone;
        cell.selectionStyle = UITableViewCellSelectionStyleBlue;
    } else {
        [cell addSubview:[self getMapView]];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    return cell;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    switch(section) {
        case 0: return nil;
        case 1: return @"Drop Pin";
        default: return nil;
    }
}

- (NSString *)tableView:(UITableView *)tableView titleForFooterInSection:(NSInteger)section
{
    switch (section) {
        case 0: return nil;
        case 1: return @"Tap the map to change the location of the pin";
        default: return nil;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.section == 1 && indexPath.row == 0) {
        return 200;
    } else {
        return [super tableView:tableView heightForRowAtIndexPath:indexPath];
    }
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.section == 0) {
        int oldSelectedIndex = selectedIndex;
        selectedIndex = indexPath.row;
        sortMode = selectedIndex;

        // deselect old
        UITableViewCell *old = [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:oldSelectedIndex inSection:0]];
        old.accessoryType = UITableViewCellAccessoryNone;
        
        // select new
        UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:indexPath];
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
        
        [tableView deselectRowAtIndexPath:indexPath animated:YES];
//        [cell setSelected:TRUE animated:TRUE];
//        [cell setSelected:FALSE animated:TRUE];
        
        if(selectedIndex == oldSelectedIndex) return;
        
        if(selectedIndex == DISTANCE_FROM_PIN) {
            [tableView insertSections:[NSIndexSet indexSetWithIndex:1]
                     withRowAnimation:UITableViewRowAnimationFade];
        } else if(oldSelectedIndex == DISTANCE_FROM_PIN) {
            [tableView deleteSections:[NSIndexSet indexSetWithIndex:1]
                     withRowAnimation:UITableViewRowAnimationFade];
        }
    } else {
        // tapped on mini map
        UIViewController *vc = [[UIViewController alloc] init];
        [self getFullMapView];
        [fullMapView clear];
        vc.view = fullMapView;
        vc.title = @"Drop Pin";
        vc.navigationItem.leftBarButtonItem = [PAUtil backButtonWithTarget:self action:@selector(didPressBackButton)];
        [self.navigationController pushViewController:vc animated:YES];
    }
}

#pragma mark - GMSMapViewDelegate

- (void)mapView:(GMSMapView *)mapView didTapAtCoordinate:(CLLocationCoordinate2D)coordinate
{
    [self updatePinLocation:coordinate];
}

- (void)mapView:(GMSMapView *)mapView didLongPressAtCoordinate:(CLLocationCoordinate2D)coordinate
{
    [self updatePinLocation:coordinate];
}

- (UIView *)mapView:(GMSMapView *)mapView markerInfoWindow:(GMSMarker *)marker
{
    return [[UIView alloc] initWithFrame:CGRectZero];
}

@end
