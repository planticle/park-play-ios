//
//  PADetailPhotosViewController.h
//  PlaygroundApp
//
//  Created by James Watmuff on 6/03/13.
//  Copyright (c) 2013 Planticle. All rights reserved.
//

#import "AQGridViewController.h"
#import "Playground.h"

@interface PADetailPhotosViewController : AQGridViewController

@property (nonatomic, strong) Playground *playground;

@end
