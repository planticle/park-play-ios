//
//  PAAboutViewController.h
//  PlaygroundApp
//
//  Created by James Watmuff on 17/05/13.
//  Copyright (c) 2013 Planticle. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <DTCoreText.h>

@class PAAboutViewController;

@protocol PAAboutViewControllerDelegate
- (void)aboutViewControllerDidFinish:(PAAboutViewController *)controller;
@end

@interface PAAboutViewController : UITableViewController <UIActionSheetDelegate, DTAttributedTextContentViewDelegate>

@property (nonatomic, weak) id <PAAboutViewControllerDelegate> delegate;

@end
