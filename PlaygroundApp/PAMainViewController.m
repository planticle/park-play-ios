//
//  PAMainViewController.m
//  PlaygroundApp
//
//  Created by James Watmuff on 27/02/13.
//  Copyright (c) 2013 Planticle. All rights reserved.
//

#import "PAMainViewController.h"
#import "PAMapViewController.h"
#import "PAListViewController.h"
#import "PAFilterViewController.h"
#import "PASortViewController.h"
#import "PAMainToolbar.h"
#import "Playground.h"
#import "PAUtil.h"

@interface PAMainViewController () {
    PAMapViewController *mapViewController;
    PAListViewController *listViewController;

    PAMainToolbar *toolbar;
    
    CLLocationManager *locationManager;
    BOOL isDefaultPin;
    
    BOOL showListOnLoad;
}

@end

@implementation PAMainViewController

@synthesize settings;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        settings = [[Settings alloc] init];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    UIImageView *titleView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"images/nav_title"]];
    if([[[UIDevice currentDevice] systemVersion] doubleValue] < 7) {
        titleView.contentMode = UIViewContentModeBottom;
        titleView.bounds = CGRectMake(0, 0, titleView.bounds.size.width, titleView.bounds.size.height + 6);
    }
    self.navigationItem.titleView = titleView;
    
    self.navigationItem.leftBarButtonItem = [PAUtil homeButtonWithTarget:self action:@selector(didPressBackButton)];
    self.navigationItem.rightBarButtonItem =  [PAUtil infoButtonWithTarget:self action:@selector(didPressAboutButton)];
    
    self.view.autoresizingMask = UIViewAutoresizingFlexibleHeight|UIViewAutoresizingFlexibleWidth;
    
    toolbar = [[PAMainToolbar alloc] initWithFrame:CGRectMake(0,0, self.view.frame.size.width, 44)];
    toolbar.autoresizingMask = UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleBottomMargin;
    [self.view addSubview:toolbar];
    
	// Do any additional setup after loading the view.
    isDefaultPin = YES;

    [toolbar.mapListToggle addTarget: self
                      action:@selector(didToggleMapList:)
            forControlEvents:UIControlEventValueChanged];
    
    [toolbar.filterButton addTarget:self
                             action:@selector(didPressFilterButton)
                   forControlEvents:UIControlEventTouchUpInside];

    
    [toolbar.sortButton addTarget:self
                           action:@selector(didPressSortButton)
                 forControlEvents:UIControlEventTouchUpInside];
    
    toolbar.sortButton.hidden = YES;

    mapViewController = [[PAMapViewController alloc] init];
    mapViewController.mainViewController = self;
    mapViewController.settings = settings;

    [self addChildViewController:mapViewController];
    [mapViewController setWantsFullScreenLayout:YES]; // prevent unwanted status bar gap

    listViewController = [[PAListViewController alloc] init];
    listViewController.mainViewController = self;
    listViewController.settings = settings;
    [listViewController setWantsFullScreenLayout:YES]; // prevent unwanted status bar gap
    [self addChildViewController:listViewController];
    
    if(showListOnLoad) {
        [self.view addSubview:listViewController.view];
        toolbar.mapListToggle.selectedSegmentIndex = 1;
        [toolbar setVisibility:YES ofButton:toolbar.sortButton];
    } else {
        [self.view addSubview:mapViewController.view];
        toolbar.mapListToggle.selectedSegmentIndex = 0;
        [toolbar setVisibility:NO ofButton:toolbar.sortButton];
    }
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(recieveShowPlaygroundOnMapNotification:) name:@"ShowPlaygroundOnMap" object:nil];
    
    
    // Fetch current location to set initial position of sort pin
    if(locationManager == nil) {
        locationManager = [[CLLocationManager alloc] init];
    }
    
    locationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters;
    locationManager.distanceFilter = 200.0f;
    locationManager.delegate = self;
    
    [locationManager startUpdatingLocation];
}

- (void)viewWillLayoutSubviews
{
    // iOS 7 Adjustments
    if([[[UIDevice currentDevice] systemVersion] doubleValue] >= 7) {
        //self.view.backgroundColor = [PAUtil yellow];
        id<UILayoutSupport> topLayoutGuide = [self topLayoutGuide];
        toolbar.frame = CGRectMake(toolbar.frame.origin.x, [topLayoutGuide length], toolbar.frame.size.width, toolbar.frame.size.height);
        [self.view bringSubviewToFront:toolbar];
    }
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self updateNavigationBarColor:animated];
    [self.navigationController setNavigationBarHidden:NO animated:animated];
}

- (void)updateNavigationBarColor:(BOOL)animated
{
    if(animated) {
        [UIView animateWithDuration:0.4f animations:^(void) {
            [self updateNavigationBarColor:NO];
        }];
        return;
    }

    UIColor *barColor = [PAUtil yellow];
    if([[[UIDevice currentDevice] systemVersion] doubleValue] >= 7) {
        UIImage *barImage = [PAUtil imageWithColor:barColor
                                       borderColor:[UIColor colorWithWhite:0 alpha:0.2]
                                              size:CGSizeMake(1, 64)
                                      borderInsets:UIEdgeInsetsMake(0, 0, 0.5, 0)];
        [self.navigationController.navigationBar setBackgroundImage:barImage forBarMetrics:UIBarMetricsDefault];
    } else {
        self.navigationController.navigationBar.backgroundColor = barColor;
    }
}

- (void)didPressBackButton
{
    CATransition *transition = [CATransition animation];
    transition.duration = 0.4;
    transition.type = kCATransitionMoveIn;
    transition.subtype = kCATransitionFromBottom;
    [self.navigationController.view.layer addAnimation:transition forKey:kCATransition];
    [self.navigationController popViewControllerAnimated:NO];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - PASortViewControllerDelegate

-(void)sortViewControllerDidFinish:(PASortViewController *)controller
{
    [self dismissViewControllerAnimated:YES completion:nil];
    settings.sortPin = controller.pin;
    settings.sortMode = controller.sortMode;
    [listViewController updatePlaygrounds];
    [listViewController.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForItem:0 inSection:0] atScrollPosition:UITableViewRowAnimationTop animated:NO];
    [listViewController.tableView reloadData];
}

#pragma mark - PAFilterViewControllerDelegate

-(void)filterViewControllerDidFinish:(PAFilterViewController *)controller
{
    settings.favouriteSelected = controller.favouriteSelected;
    settings.ourFavouriteSelected = controller.ourFavouriteSelected;
    [self dismissViewControllerAnimated:YES completion:nil];
//    int count = selectedEquipment.count + selectedFacilities.count;
//    NSString *filterButtonLabel = (count > 0) ? [NSString stringWithFormat:@"Filter (%d)", count] : @"Filter";
//    filterButton.title = filterButtonLabel;
}

#pragma mark - PAAboutViewControllerDelegate

-(void)aboutViewControllerDidFinish:(PAAboutViewController *)controller
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - CLLocationManagerDelegate
-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    CLLocation *location = locations.lastObject;

    if(isDefaultPin) {
        settings.sortPin = location.coordinate;
    } else {
        [manager stopUpdatingLocation];
    }
}

#pragma mark - Custom methods

- (void)setSortPin:(CLLocationCoordinate2D)aSortPin
{
    isDefaultPin = NO;
    settings.sortPin = aSortPin;
}

- (void)didPressFilterButton
{
    PAFilterViewController *filterViewController = [[PAFilterViewController alloc] initWithStyle:UITableViewStyleGrouped];
    filterViewController.selectedFacilities = settings.selectedFacilities;
    filterViewController.selectedEquipment = settings.selectedEquipment;
    filterViewController.favouriteSelected = settings.favouriteSelected;
    filterViewController.ourFavouriteSelected = settings.ourFavouriteSelected;
    filterViewController.delegate = self;
    
    UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:filterViewController];
    navController.navigationBar.tintColor = self.navigationController.navigationBar.tintColor;
    [self presentViewController:navController animated:YES completion:nil];
}

- (void)didPressAboutButton
{
    PAAboutViewController *aboutViewController = [[PAAboutViewController alloc] initWithStyle:UITableViewStyleGrouped];
    aboutViewController.delegate = self;
    
    UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:aboutViewController];
    if([[[UIDevice currentDevice] systemVersion] doubleValue] < 7) {
        navController.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
    }
    navController.navigationBar.tintColor = self.navigationController.navigationBar.tintColor;
    
    [self presentViewController:navController animated:YES completion:nil];
}

- (void)didPressSortButton
{
    PASortViewController *sortViewController = [[PASortViewController alloc] initWithStyle:UITableViewStyleGrouped];
    sortViewController.pin = settings.sortPin;
    sortViewController.sortMode = settings.sortMode;
    sortViewController.delegate = self;

    UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:sortViewController];
    navController.navigationBar.tintColor = self.navigationController.navigationBar.tintColor;

    [self presentViewController:navController animated:YES completion:nil];
}


- (void)didToggleMapList:(UISegmentedControl *)control
{
    switch(control.selectedSegmentIndex) {
        case 0:
            [listViewController.view removeFromSuperview];
            [self.view addSubview:mapViewController.view];
            //mapViewController.view.frame = self.view.bounds;
            //[self.navigationItem setRightBarButtonItems:@[spacer, aboutButton] animated:YES];
            [toolbar setVisibility:NO ofButton:toolbar.sortButton];
            break;
        case 1:
            [mapViewController.view removeFromSuperview];
            [self.view addSubview:listViewController.view];
            // TODO - update list view controller and remove following line
            //listViewController.view.frame = self.view.bounds;
            //[self.navigationItem setRightBarButtonItems:@[sortButton] animated:YES];
            [toolbar setVisibility:YES ofButton:toolbar.sortButton];
            break;
    }
}

- (void)showMap
{
    showListOnLoad = NO;
    if(toolbar.mapListToggle && toolbar.mapListToggle.selectedSegmentIndex == 1) {
        // simulate toggle to map
        toolbar.mapListToggle.selectedSegmentIndex = 0;
        [self didToggleMapList:toolbar.mapListToggle];
    }
}

- (void)showList
{
    showListOnLoad = YES;
    if(toolbar.mapListToggle && toolbar.mapListToggle.selectedSegmentIndex == 0) {
        // simulate toggle to map
        toolbar.mapListToggle.selectedSegmentIndex = 1;
        [self didToggleMapList:toolbar.mapListToggle];
    }
}

- (void)recieveShowPlaygroundOnMapNotification:(NSNotification *)notification
{
    Playground *playground = notification.object;

    [self showMap];
    [self.navigationController popToViewController:self animated:YES];
    [mapViewController selectPlayground:playground];

}

@end
