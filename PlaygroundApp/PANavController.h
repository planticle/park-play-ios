//
//  PANavController.h
//  PlaygroundApp
//
//  Created by James Watmuff on 27/02/13.
//  Copyright (c) 2013 Planticle. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PANavController : UINavigationController

@end
