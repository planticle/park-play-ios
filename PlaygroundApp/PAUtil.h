//
//  PAUtil.h
//  PlaygroundApp
//
//  Created by James Watmuff on 20/05/13.
//  Copyright (c) 2013 Planticle. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Facility.h"
#import "Equipment.h"
#import <CoreLocation/CoreLocation.h>

@interface PAUtil : NSObject
+ (UIImage *)iconForFacility:(Facility *)facility;
+ (UIImage *)iconForEquipment:(Equipment *)equipment;
+ (NSArray *)sortPlaygrounds:(NSArray *)playgrounds byDistanceFromLocation:(CLLocation *)location;
+ (NSInteger)numberOfCores;
+ (UIImage *)imageWithColor:(UIColor *)color size:(CGSize)size;
+ (UIImage *)imageWithColor:(UIColor *)color;
+ (UIImage *)imageWithColor:(UIColor *)color borderColor:(UIColor *)borderColor size:(CGSize)size borderInsets:(UIEdgeInsets)borderInsets;
+ (UIImage *)resizableImageWithColor:(UIColor *)color borderColor:(UIColor *)borderColor borderInsets:(UIEdgeInsets)borderInsets;
+ (UIImage *)resizableImageWithColor:(UIColor *)color borderColor:(UIColor *)borderColor borderInsets:(UIEdgeInsets)borderInsets paddingInsets:(UIEdgeInsets)paddingInsets;
+ (UIButton *)navButtonWithTitle:(NSString *)title;
+ (UIBarButtonItem *)barButtonItemWithTitle:(NSString *)title target:(id)target action:(SEL)selector;
+ (UIBarButtonItem *)barButtonItemWithImage:(UIImage *)image target:(id)target action:(SEL)selector;
+ (UIBarButtonItem *)backButtonWithTarget:(id)target action:(SEL)selector;
+ (UIBarButtonItem *)infoButtonWithTarget:(id)target action:(SEL)selector;
+ (UIBarButtonItem *)homeButtonWithTarget:(id)target action:(SEL)selector;

+ (UIColor *)green;
+ (UIColor *)darkGreen;
+ (UIColor *)lightGreen;
+ (UIColor *)red;
+ (UIColor *)darkRed;
+ (UIColor *)yellow;
+ (UIColor *)orange;
@end
