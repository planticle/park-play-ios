//
//  PADetailPhotosViewController.m
//  PlaygroundApp
//
//  Created by James Watmuff on 6/03/13.
//  Copyright (c) 2013 Planticle. All rights reserved.
//

#import "PADetailPhotosViewController.h"
#import "PAPhotoGridViewCell.h"
#import "UIImageView+AFNetworking.h"
#import "Photo.h"

@interface PADetailPhotosViewController () {
    NSArray *photos;
    
    // State for showing & hiding enlarged image
    UIView *enlargeView;
}
@property UIImageView *imageView;

@end

@implementation PADetailPhotosViewController

@synthesize playground;
@synthesize imageView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.

    //self.title = playground.name;
    self.title = @"Photos";
//    self.navigationController.navigationBar.titleTextAttributes = @{UITextAttributeFont: [UIFont fontWithName:@"AmericanTypewriter" size:16.0]};
    
    self.gridView.backgroundView = nil;
    self.gridView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"images/bg"]];

    photos = [playground.photos sortedArrayUsingDescriptors:@[[NSSortDescriptor sortDescriptorWithKey:@"name" ascending:YES selector:@selector(caseInsensitiveCompare:)]]];

    [self.gridView reloadData];
}

- (NSURL *)URLForPhoto:(Photo *)photo thumbnail:(BOOL)thumb
{
    NSString *type = thumb ? @"thumb" : @"large";
    NSString *URLstr = [NSString stringWithFormat:@"https://s3-ap-southeast-2.amazonaws.com/goplay.planticle.com.au/%@/%@/%@", type, photo.playground.name, photo.name];
    URLstr = [URLstr stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    return [NSURL URLWithString:URLstr];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark -
#pragma mark Grid View Data Source

- (NSUInteger)numberOfItemsInGridView:(AQGridView *)gridView
{
    return playground.photos.count;
}

- (AQGridViewCell *)gridView:(AQGridView *)gridView cellForItemAtIndex:(NSUInteger)index
{
    static NSString *CellIdentifier = @"GridViewCell";
    
    PAPhotoGridViewCell *cell = [[PAPhotoGridViewCell alloc] initWithFrame:CGRectMake(0,0,140,140) reuseIdentifier:CellIdentifier];
    cell.selectionStyle = AQGridViewCellSelectionStyleNone;
    
    Photo *photo = photos[index];
    [cell setImageURL:[self URLForPhoto:photo thumbnail:YES]];

    BOOL awesome = [photo.name rangeOfString:@"awesome" options:NSCaseInsensitiveSearch].location != NSNotFound;
    [cell setRibbonDisplayed:awesome];

    return cell;
}

- (CGSize)portraitGridCellSizeForGridView:(AQGridView *)gridView
{
    return CGSizeMake(160, 160);
}

- (void)gridView:(AQGridView *)gridView didSelectItemAtIndex:(NSUInteger)index
{
    Photo *photo = photos[index];
    
    UIView *parentView = self.parentViewController.view;
    enlargeView = [[UIView alloc] initWithFrame:parentView.bounds];
    imageView = [[UIImageView alloc] init];
    
    __weak typeof(self) weakSelf = self;
    UIImageView *localImageView = imageView;
    [imageView setImageWithURLRequest:[NSURLRequest requestWithURL:[self URLForPhoto:photo thumbnail:NO]]
                     placeholderImage:[UIImage imageNamed:@"images/large_placeholder"]
                              success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image) {
                                  localImageView.frame = [weakSelf centeredFitOf:image.size within:parentView.bounds.size];
                              }
                              failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error) {
                              }
     ];
 
    CGRect selectedCellFrame = [self.gridView cellForItemAtIndex:index].frame;
    selectedCellFrame = [self.view convertRect:selectedCellFrame toView:parentView];
    imageView.frame = selectedCellFrame;
    
    imageView.alpha = 0.0;
    enlargeView.backgroundColor = [UIColor colorWithWhite:0 alpha:0];
    
    enlargeView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    
    [enlargeView addSubview:imageView];
    [parentView addSubview:enlargeView];
    
    [UIView animateWithDuration:0.5
                     animations:^{
                        imageView.frame = [self centeredFitOf:imageView.image.size within:parentView.bounds.size];
                        imageView.alpha = 1.0;
                        enlargeView.backgroundColor = [UIColor colorWithWhite:0 alpha:1.0];
                     }
                     completion:^(BOOL finished) {
                         // Hide navigation bar to avoid glitches during orientation changes
                         self.navigationController.navigationBarHidden = YES;
                     }
     ];
    
    UITapGestureRecognizer *gestureRecogniser = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleImageViewTap:)];
    [enlargeView addGestureRecognizer:gestureRecogniser];
}

- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    imageView.frame = [self centeredFitOf:imageView.image.size within:self.parentViewController.view.bounds.size];
}

- (void) handleImageViewTap:(UIGestureRecognizer*)tap
{
    self.navigationController.navigationBarHidden = NO;
    // Ensure enlarge view is still at the front
    [enlargeView.superview bringSubviewToFront:enlargeView];

    CGRect selectedCellFrame = [self.gridView cellForItemAtIndex:self.gridView.indexOfSelectedItem].frame;
    selectedCellFrame = [self.view convertRect:selectedCellFrame toView:self.parentViewController.view];

    [UIView animateWithDuration:0.5
                     animations:^{
                         imageView.frame = selectedCellFrame;
                         imageView.alpha = 0.0;
                         enlargeView.backgroundColor = [UIColor colorWithWhite:0 alpha:0];
                     }
                     completion:^(BOOL finished){
                         [enlargeView removeFromSuperview];
                         [self.gridView deselectItemAtIndex:self.gridView.indexOfSelectedItem animated:YES];
                     }
     ];
}



- (CGRect)centeredFitOf:(CGSize)size within:(CGSize)bounds
{
    CGRect rect;
    if(size.width / size.height > bounds.width / bounds.height) {
        rect.size.width = bounds.width;
        rect.size.height = size.height * bounds.width / size.width;
    } else {
        rect.size.height = bounds.height;
        rect.size.width = size.width * bounds.height / size.height;
    }
    
    rect.origin.x = (bounds.width - rect.size.width) / 2;
    rect.origin.y = (bounds.height - rect.size.height) / 2;
    
    return rect;
}

@end
