//
//  PAListViewController.h
//  PlaygroundApp
//
//  Created by James Watmuff on 27/02/13.
//  Copyright (c) 2013 Planticle. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import "PAMainViewController.h"
#import "Settings.h"

@class PAMainViewController; // forward dec

@interface PAListViewController : UITableViewController <NSFetchedResultsControllerDelegate, CLLocationManagerDelegate> {
    CLLocationManager *locationManager;
}

- (void)updatePlaygrounds;

@property (nonatomic, strong) PAMainViewController *mainViewController;
@property (nonatomic, strong) Settings *settings;

@end
