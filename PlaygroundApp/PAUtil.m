//
//  PAUtil.m
//  PlaygroundApp
//
//  Created by James Watmuff on 20/05/13.
//  Copyright (c) 2013 Planticle. All rights reserved.
//

#import "PAUtil.h"
#import "Playground+Extra.h"
#include <sys/sysctl.h>

unsigned int countCores();

@implementation PAUtil
+(UIImage *)iconForEquipment:(Equipment *)equipment
{
    return [PAUtil iconNamed:equipment.name];
}

+(UIImage *)iconForFacility:(Facility *)facility
{
    return [PAUtil iconNamed:facility.name];
}

+(UIImage *)iconNamed:(NSString *)name
{
    return [UIImage imageNamed:[NSString stringWithFormat:@"images/icons/%@", name]];
}

+ (NSArray *)sortPlaygrounds:(NSArray *)playgrounds byDistanceFromLocation:(CLLocation *)location
{
    return [playgrounds sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2) {
        CLLocation *l1 = ((Playground *)obj1).location;
        CLLocation *l2 = ((Playground *)obj2).location;
        CLLocationDistance d1 = [l1 distanceFromLocation:location];
        CLLocationDistance d2 = [l2 distanceFromLocation:location];
        return d2 > d1 ? NSOrderedAscending :
        d2 < d1 ? NSOrderedDescending : NSOrderedSame;
    }];
}

+ (NSInteger)numberOfCores
{
    return (NSInteger)countCores();
}

+ (UIImage *)imageWithColor:(UIColor *)color size:(CGSize)size
{
    CGRect rect = CGRectMake(0, 0, size.width, size.height);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return img;
}

+ (UIImage *)imageWithColor:(UIColor *)color
{
    return [PAUtil imageWithColor:color size:CGSizeMake(1,1)];
}

+ (UIImage *)imageWithColor:(UIColor *)color borderColor:(UIColor *)borderColor size:(CGSize)size borderInsets:(UIEdgeInsets)borderInsets
{
    return [PAUtil imageWithColor:color borderColor:borderColor size:size borderInsets:borderInsets paddingInsets:UIEdgeInsetsZero];
}

+ (UIImage *)imageWithColor:(UIColor *)color borderColor:(UIColor *)borderColor size:(CGSize)size borderInsets:(UIEdgeInsets)borderInsets paddingInsets:(UIEdgeInsets)paddingInsets
{
    CGRect rect = CGRectMake(0, 0, size.width, size.height);

    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, UIEdgeInsetsInsetRect(rect, paddingInsets));
    
    CGContextSetFillColorWithColor(context, [borderColor CGColor]);
    // Left border
    CGContextFillRect(context, CGRectMake(paddingInsets.left,
                                          paddingInsets.top,
                                          borderInsets.left,
                                          size.height - paddingInsets.bottom - paddingInsets.top));
    // Right border
    CGContextFillRect(context, CGRectMake(size.width - borderInsets.right - paddingInsets.right,
                                          paddingInsets.top,
                                          borderInsets.right,
                                          size.height - paddingInsets.bottom - paddingInsets.top));
    // Top border
    CGContextFillRect(context, CGRectMake(borderInsets.left + paddingInsets.left,
                                          paddingInsets.top,
                                          size.width - borderInsets.left - borderInsets.right - paddingInsets.left - paddingInsets.right,
                                          borderInsets.top));
    // Bottom border
    CGContextFillRect(context, CGRectMake(borderInsets.left + paddingInsets.left,
                                          size.height - borderInsets.bottom - paddingInsets.bottom,
                                          size.width - borderInsets.left - borderInsets.right - paddingInsets.left - paddingInsets.right,
                                          borderInsets.bottom));
    
    UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return img;
}

+ (UIImage *)resizableImageWithColor:(UIColor *)color borderColor:(UIColor *)borderColor borderInsets:(UIEdgeInsets)borderInsets
{
    return [self resizableImageWithColor:color
                             borderColor:borderColor
                            borderInsets:borderInsets
                           paddingInsets:UIEdgeInsetsZero];
}

+ (UIImage *)resizableImageWithColor:(UIColor *)color borderColor:(UIColor *)borderColor borderInsets:(UIEdgeInsets)borderInsets paddingInsets:(UIEdgeInsets)paddingInsets
{
    CGSize size = CGSizeMake(borderInsets.left + borderInsets.right + paddingInsets.left + paddingInsets.right + 1,
                             borderInsets.bottom + borderInsets.top + paddingInsets.bottom + paddingInsets.top + 1);

    UIEdgeInsets capInsets = UIEdgeInsetsMake(borderInsets.top + paddingInsets.top,
                                              borderInsets.left + paddingInsets.left,
                                              borderInsets.bottom + paddingInsets.bottom,
                                              borderInsets.right + paddingInsets.right);

    UIImage *image = [PAUtil imageWithColor:color
                       borderColor:borderColor
                              size:size
                               borderInsets:borderInsets
                              paddingInsets:paddingInsets];

    return [image resizableImageWithCapInsets:capInsets];
}

+ (UIButton *)navButtonWithTitle:(NSString *)title
{
    UIButton *button = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 80, 44)];
    [button setTitle:title forState:UIControlStateNormal];
    button.titleLabel.font = [UIFont fontWithName:@"American Typewriter" size:18.0];
    button.titleLabel.textColor = [UIColor whiteColor];
    return button;
}

+ (UIBarButtonItem *)barButtonItemWithTitle:(NSString *)title target:(id)target action:(SEL)selector
{
    UIButton *button = [self navButtonWithTitle:title];
    [button addTarget:target action:selector forControlEvents:UIControlEventTouchUpInside];
    return [[UIBarButtonItem alloc] initWithCustomView:button];
}

+ (UIBarButtonItem *)backButtonWithTarget:(id)target action:(SEL)selector
{
    UIButton *button = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 74, 44)];
    [button setImage:[UIImage imageNamed:@"images/back"] forState:UIControlStateNormal];
    [button setBackgroundImage:[UIImage imageNamed:nil] forState:UIControlStateNormal];
    [button addTarget:target action:selector forControlEvents:UIControlEventTouchUpInside];
    return [[UIBarButtonItem alloc] initWithCustomView:button];
}

+ (UIBarButtonItem *)barButtonItemWithImage:(UIImage *)image target:(id)target action:(SEL)selector
{
    UIButton *button = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, image.size.width * 44 / image.size.height, 44)];
    [button setImage:image forState:UIControlStateNormal];
    [button setBackgroundImage:[UIImage imageNamed:nil] forState:UIControlStateNormal];
    [button addTarget:target action:selector forControlEvents:UIControlEventTouchUpInside];
    return [[UIBarButtonItem alloc] initWithCustomView:button];
}

+ (UIBarButtonItem *)homeButtonWithTarget:(id)target action:(SEL)selector
{
    UIButton *button = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 44, 44)];
    [button setImage:[UIImage imageNamed:@"images/home"] forState:UIControlStateNormal];
    [button setBackgroundImage:[UIImage imageNamed:nil] forState:UIControlStateNormal];
    [button addTarget:target action:selector forControlEvents:UIControlEventTouchUpInside];
    return [[UIBarButtonItem alloc] initWithCustomView:button];
}

+ (UIBarButtonItem *)infoButtonWithTarget:(id)target action:(SEL)selector
{
    UIButton *button = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 47, 44)];
    [button setImage:[UIImage imageNamed:@"images/info"] forState:UIControlStateNormal];
    [button setBackgroundImage:[UIImage imageNamed:nil] forState:UIControlStateNormal];
    [button addTarget:target action:selector forControlEvents:UIControlEventTouchUpInside];
    return [[UIBarButtonItem alloc] initWithCustomView:button];
}

+ (UIColor *)green
{
    return [UIColor colorWithRed:138.0/255.0 green:195.0/255.0 blue:179.0/255.0 alpha:1.0];
}

+ (UIColor *)darkGreen
{
    return [UIColor colorWithRed:128.0/255.0 green:185.0/255.0 blue:169.0/255.0 alpha:1.0];
}

+ (UIColor *)lightGreen
{
    return [UIColor colorWithRed:148.0/255.0 green:205.0/255.0 blue:189.0/255.0 alpha:1.0];
}

+ (UIColor *)red
{
    return [UIColor colorWithRed:218./255.0 green:109./255.0 blue:88./255.0 alpha:1.0];
}

+ (UIColor *)darkRed
{
    return [UIColor colorWithRed:198./255.0 green:89./255.0 blue:68./255.0 alpha:1.0];
}

+ (UIColor *)yellow
{
    return [UIColor colorWithRed:223.f/255.f green:213.f/255.f blue:162.f/255.f alpha:1.0];
}

+ (UIColor *)orange
{
    return [UIColor colorWithRed:249.0/255 green:170.0/255 blue:58.0/255 alpha:1];
}

@end


static unsigned int ncpu = 0;
unsigned int countCores()
{
    if(ncpu == 0) {
        size_t len;
        
        len = sizeof(ncpu);
        sysctlbyname ("hw.ncpu",&ncpu,&len,NULL,0);
        
        NSLog(@"Number of cores: %d", ncpu);
    }
    return ncpu;
}