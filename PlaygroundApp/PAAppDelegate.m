//
//  PAAppDelegate.m
//  PlaygroundApp
//
//  Created by James Watmuff on 27/02/13.
//  Copyright (c) 2013 Planticle. All rights reserved.
//

#import "PAAppDelegate.h"
#import "PAMainViewController.h"
#import "PANavController.h"
#import "PAHomeViewController.h"

#import <GoogleMaps/GoogleMaps.h>
#import <GAI.h>

#import "Playground.h"
#import "Facility.h"
#import "Equipment.h"
#import "PlaygroundFacility.h"
#import "PlaygroundEquipment.h"
#import "Photo.h"
#import "PADataImporter.h"
#import "PAUtil.h"
#import <AFNetworkActivityIndicatorManager.h>

#import <iRate.h>

@implementation PAAppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    // show the status bar (it is hidden during launch to avoid an iPad iOS6 bug when launching in landscape orientation
    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationNone];

    [self customiseAppearance];
    [self setupGoogleAnalytics];
    
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    // Override point for customization after application launch.
    [GMSServices provideAPIKey:@"AIzaSyDM3jArqMJ8AtPkPyDsj9Z_pBEc-mrz8A8"];

    [self initOrUpdateDatabase];
    
    PAHomeViewController *homeViewController = [[PAHomeViewController alloc] init];
    
//    PAMainViewController *mainViewController = [[PAMainViewController alloc] init];
    
    PANavController *navViewController = [[PANavController alloc] initWithRootViewController:homeViewController];
    
    self.window.rootViewController = navViewController;
    
    self.window.backgroundColor = [UIColor whiteColor];
    [self.window makeKeyAndVisible];
    
    [[AFNetworkActivityIndicatorManager sharedManager] setEnabled:YES];
    
    return YES;
}

+ (void)initialize
{
    [iRate sharedInstance].daysUntilPrompt = 5;
    [iRate sharedInstance].usesUntilPrompt = 5;
}

-(void)setupGoogleAnalytics
{
    // Optional: automatically send uncaught exceptions to Google Analytics.
    [GAI sharedInstance].trackUncaughtExceptions = YES;
    // Optional: set debug to YES for extra debugging information.
    //[GAI sharedInstance].debug = YES;
    // Create tracker instance.
    id<GAITracker> tracker = [[GAI sharedInstance] trackerWithTrackingId:@"UA-41799926-2"];

    [tracker sendEventWithCategory:@"application"
                        withAction:@"launch"
                         withLabel:@"OK"
                         withValue:nil];
}

-(void)customiseAppearance
{

    UIColor *barColor = [PAUtil red];//[UIColor colorWithRed:218./255.0 green:109./255.0 blue:88./255.0 alpha:1.0];
    UIColor *textColor = [UIColor whiteColor];
    
    if([[[UIDevice currentDevice] systemVersion] doubleValue] < 7) {
        [[UINavigationBar appearance] setTitleVerticalPositionAdjustment:-3.f forBarMetrics:UIBarMetricsDefault];
        UIImage *barImage = [PAUtil imageWithColor:[UIColor clearColor]
                                       borderColor:[UIColor colorWithWhite:0 alpha:0.2]
                                              size:CGSizeMake(1, 44)
                                      borderInsets:UIEdgeInsetsMake(0, 0, 0.5, 0)];
        [[UINavigationBar appearance] setBackgroundImage:barImage forBarMetrics:UIBarMetricsDefault];
        [[UINavigationBar appearance] setBackgroundColor:barColor];
    } else {
        UIImage *barImage = [PAUtil imageWithColor:barColor
                                       borderColor:[UIColor colorWithWhite:0 alpha:0.2]
                                              size:CGSizeMake(1, 64)
                                      borderInsets:UIEdgeInsetsMake(0, 0, 0.5, 0)];
        [[UINavigationBar appearance] setBackgroundImage:barImage forBarMetrics:UIBarMetricsDefault];
    }

    [[UINavigationBar appearance] setTitleTextAttributes:@{
                                     UITextAttributeFont: [UIFont fontWithName:@"AmericanTypewriter" size:24.0],
                                UITextAttributeTextColor: textColor,
                          UITextAttributeTextShadowColor: [UIColor clearColor],
                         UITextAttributeTextShadowOffset: [NSValue valueWithUIOffset:UIOffsetMake(0, 1)]
     }];
    [[UINavigationBar appearance] setShadowImage:[[UIImage alloc] init]];
    
    UIButton *barButtonItemAppearance = [UIButton appearanceWhenContainedIn:[UINavigationBar class], nil];

    UIColor *buttonColor = [UIColor colorWithWhite:0 alpha:0.1];
    UIColor *darkButtonColor = [UIColor colorWithWhite:0 alpha:0.3];
    [barButtonItemAppearance setBackgroundImage:[PAUtil imageWithColor:buttonColor]
                                       forState:UIControlStateNormal];
    [barButtonItemAppearance setBackgroundImage:[PAUtil imageWithColor:darkButtonColor]
                                       forState:UIControlStateHighlighted];
}

-(void)initOrUpdateDatabase
{
    // test if database exists
    NSFileManager *fileMgr = [NSFileManager defaultManager];

    PADataImporter *importer = [PADataImporter sharedInstance];

    // load local JSON file
    NSURL *jsonURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"data.json"];
  
    // if local JSON file missing, use bundled JSON file
    if(![fileMgr fileExistsAtPath:[jsonURL path]]) {
        jsonURL = [[NSBundle mainBundle] URLForResource:@"data" withExtension:@"json"];
    }

    NSData *data = [fileMgr contentsAtPath:[jsonURL path]];
    NSDictionary *jsonData = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
    
    // populate database
    [importer updateDataWithJSONData:jsonData];

    // uncomment to force update:
    //NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    //[userDefaults setInteger:-1 forKey:@"lastUpdateTimestamp"];
    
    [importer checkForUpdate];
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    [[PADataImporter sharedInstance] checkForUpdate];
    
    [[[GAI sharedInstance] defaultTracker] sendEventWithCategory:@"application"
                                                      withAction:@"willEnterForeground"
                                                       withLabel:@"OK"
                                                       withValue:nil];

    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
}

#pragma mark - Application's Documents directory

// Returns the URL to the application's Documents directory.
- (NSURL *)applicationDocumentsDirectory
{
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

@end
