//
//  PAMainToolbar.m
//  PlaygroundApp
//
//  Created by James Watmuff on 25/07/13.
//  Copyright (c) 2013 Planticle. All rights reserved.
//

#import "PAMainToolbar.h"
#import "PAUtil.h"
#import <QuartzCore/QuartzCore.h>

@implementation PAMainToolbar

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self createSubviews];
    }
    return self;
}

@synthesize mapListToggle;
@synthesize filterButton;
@synthesize sortButton;

- (void)createSubviews
{
    UIColor *greenColor = [PAUtil green];
    UIColor *lightGreenColor = [PAUtil lightGreen];
    UIColor *darkGreenColor = [PAUtil darkGreen];
    
    UIImage *darkToggleImage = [PAUtil resizableImageWithColor:darkGreenColor
                                                   borderColor:darkGreenColor
                                                  borderInsets:UIEdgeInsetsMake(2, 2, 2, 2)
                                                 paddingInsets:UIEdgeInsetsMake(5, 0, 5, 0)];

    UIImage *lightToggleImage = [PAUtil resizableImageWithColor:lightGreenColor
                                                    borderColor:darkGreenColor
                                                   borderInsets:UIEdgeInsetsMake(2, 2, 2, 2)
                                                  paddingInsets:UIEdgeInsetsMake(5, 0, 5, 0)];
    NSDictionary *textAttributes = @{
                                     UITextAttributeFont: [UIFont fontWithName:@"American Typewriter" size:18.0],
                                     UITextAttributeTextColor: [UIColor whiteColor],
                                     UITextAttributeTextShadowColor: [UIColor clearColor],
                                     UITextAttributeTextShadowOffset: [NSValue valueWithUIOffset:UIOffsetMake(0, 1)]
                                     };
    
    // -- Map List Toggle
    
    mapListToggle = [[UISegmentedControl alloc] initWithItems:@[@"Map", @"List"]];
    mapListToggle.selectedSegmentIndex = 0;
    mapListToggle.segmentedControlStyle = UISegmentedControlStylePlain;

    // ---- Geometry

    [mapListToggle setWidth:75 forSegmentAtIndex:0];
    [mapListToggle setWidth:74 forSegmentAtIndex:1];
    mapListToggle.frame = CGRectMake(85, 0, 150, 44);
    
    // ---- Colours
    [mapListToggle setDividerImage:[PAUtil resizableImageWithColor:darkGreenColor borderColor:nil borderInsets:UIEdgeInsetsZero paddingInsets:UIEdgeInsetsMake(5,0,5,0)]
               forLeftSegmentState:UIControlStateNormal
                 rightSegmentState:UIControlStateNormal
                        barMetrics:UIBarMetricsDefault];

    [mapListToggle setTitleTextAttributes:textAttributes forState:UIControlStateNormal];
    [mapListToggle setTitleTextAttributes:textAttributes forState:UIControlStateSelected];
    [mapListToggle setBackgroundImage:darkToggleImage forState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
    [mapListToggle setBackgroundImage:lightToggleImage forState:UIControlStateSelected barMetrics:UIBarMetricsDefault];
    
    // -- Filter Button
    filterButton = [self createButtonWithTitle:@"Filter" frame:CGRectMake(0,0,80,44)];

    // -- Sort Button
    sortButton = [self createButtonWithTitle:@"Sort" frame:CGRectMake(240,0,80,44)];

    [self setBackgroundColor:greenColor];
//    [self setBackgroundImage:[PAUtil imageWithColor:greenColor] forToolbarPosition:UIToolbarPositionAny barMetrics:UIBarMetricsDefault];
    
    [self addSubview:filterButton];
    [self addSubview:sortButton];
    [self addSubview:mapListToggle];
    
    // -- Bottom darkened border
    CAShapeLayer *borderLayer = [CAShapeLayer layer];
    CGRect borderFrame = CGRectIntersection(self.bounds, CGRectOffset(self.bounds, 0, self.bounds.size.height - 1));
    [borderLayer setFrame:borderFrame];
    [borderLayer setBackgroundColor:[[UIColor colorWithWhite:0 alpha:0.1] CGColor]];
    [self.layer addSublayer:borderLayer];
}

- (UIButton *)createButtonWithTitle:(NSString *)title frame:(CGRect)frame
{
    UIColor *redColor = [PAUtil red];
    UIColor *darkRedColor = [PAUtil darkRed];

    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.frame = frame;
    [button setBackgroundImage:[PAUtil imageWithColor:redColor] forState:UIControlStateNormal];
    [button setBackgroundImage:[PAUtil imageWithColor:darkRedColor] forState:UIControlStateHighlighted];
    [button setTitle:title forState:UIControlStateNormal];
    button.titleLabel.font = [UIFont fontWithName:@"American Typewriter" size:18.0];
    button.titleLabel.textColor = [UIColor whiteColor];

    return button;
}

- (void) setVisibility:(BOOL)visibility ofButton:(UIButton *)button
{
    CATransition *animation = [CATransition animation];
    animation.type = kCATransitionFade;
    animation.duration = 0.3;
    [button.layer addAnimation:animation forKey:nil];
    
    button.hidden = !visibility;
}

@end
