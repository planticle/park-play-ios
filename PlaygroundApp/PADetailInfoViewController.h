//
//  PADetailFacilitiesViewController.h
//  PlaygroundApp
//
//  Created by James Watmuff on 4/03/13.
//  Copyright (c) 2013 Planticle. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Playground;

@interface PADetailInfoViewController : UITableViewController <UITableViewDelegate, UITableViewDataSource, UIActionSheetDelegate>

@property (nonatomic, strong) Playground *playground;

@end
