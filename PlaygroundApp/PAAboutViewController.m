//
//  PAAboutViewController.m
//  PlaygroundApp
//
//  Created by James Watmuff on 17/05/13.
//  Copyright (c) 2013 Planticle. All rights reserved.
//

#import "PAAboutViewController.h"
#import "GMSServices.h"
#import "PAData.h"
#import "PAUtil.h"
#import <DTCoreText.h>

typedef NS_ENUM(NSUInteger, AboutSectionType) {
    kAbout,
    kFeedback,
    kInfo
};

typedef NS_ENUM(NSUInteger, AboutRowType) {
    kAboutUs,
    kAboutApp,
    kAcknowledgements
};

typedef NS_ENUM(NSUInteger, FeedbackRowType) {
    kCorrection,
    kSuggestion
};

typedef NS_ENUM(NSUInteger, InfoRowType) {
    kVersion,
    kNumberOfPlaygrounds,
    kLegal
};

@interface PAAboutViewController () {
    NSString *destinationURL;
}
@end

@implementation PAAboutViewController

@synthesize delegate;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
//    UIBarButtonItem *rightButton = [[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(didPressDoneButton)];
    UIBarButtonItem *rightButton = [PAUtil barButtonItemWithTitle:@"Done" target:self action:@selector(didPressDoneButton)];
    
    self.navigationItem.rightBarButtonItem = rightButton;
    self.title = @"About";
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)didPressDoneButton
{
    [delegate aboutViewControllerDidFinish:self];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 3;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    switch((AboutSectionType)section) {
        case kAbout: return 3;
        case kFeedback: return 2;
        case kInfo: return 3;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell;

    switch((AboutSectionType)indexPath.section) {
        case kAbout:
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            switch((AboutRowType)indexPath.row) {
                case kAboutUs:
                    cell.textLabel.text = @"About Us";
                    break;
                case kAboutApp:
                    cell.textLabel.text = @"About This App";
                    break;
                case kAcknowledgements:
                    cell.textLabel.text = @"Acknowledgements";
            }
            break;

        case kFeedback:
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
            cell.textLabel.textAlignment = NSTextAlignmentCenter;

            switch((FeedbackRowType)indexPath.row) {
                case kCorrection:
                    cell.textLabel.text = @"Report Incorrect Information";
                    break;
                case kSuggestion:
                    cell.textLabel.text = @"Suggest New Playground";
                    break;
            }
            break;
        case kInfo:
            switch((InfoRowType)indexPath.row) {
                case kVersion:
                    cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:nil];
                    cell.textLabel.text = @"Version";
                    cell.detailTextLabel.text = @"1.2";
                    cell.selectionStyle = UITableViewCellSelectionStyleNone;
                    break;
                case kNumberOfPlaygrounds:
                    cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:nil];
                    cell.textLabel.text = @"Playgrounds Listed";
                    cell.detailTextLabel.text = [NSString stringWithFormat:@"%d", [PAData sharedInstance].playgrounds.count];
                    cell.selectionStyle = UITableViewCellSelectionStyleNone;
                    break;
                case kLegal:
                    cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
                    cell.textLabel.text = @"Legal Notices";
                    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
                    break;
            }
            break;
    }
    
    return cell;
}

-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    switch((AboutSectionType)section) {
        case kAbout: return nil;
        case kFeedback: return @"Feedback";
        case kInfo: return @"Information";
    }
}

-(NSString *)tableView:(UITableView *)tableView titleForFooterInSection:(NSInteger)section
{
    if(section == [self numberOfSectionsInTableView:tableView] - 1) {
        return @"© 2014 Planticle";
    } else {
        return nil;
    }
}

- (NSString *)processHtml:(NSString *)html
{
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:@"bundle:([.a-zA-Z0-9/_-]+)" options:nil error:nil];
    NSMutableString *processedHtml = [[NSMutableString alloc] init];
    NSInteger __block lastMatchIndex = 0;
    [regex enumerateMatchesInString:html options:nil range:NSMakeRange(0, [html length]) usingBlock:^(NSTextCheckingResult *result, NSMatchingFlags flags, BOOL *stop) {
        // Copy non-matching part of string so far
        [processedHtml appendString:[html substringWithRange:NSMakeRange(lastMatchIndex, result.range.location - lastMatchIndex)]];
        
        // Calculate URL
        NSString *path = [html substringWithRange:[result rangeAtIndex:1]];
        NSString *imageName = [[path lastPathComponent] stringByDeletingPathExtension];
        NSString *extension = [path pathExtension];
        NSString *subdirectory = [path stringByDeletingLastPathComponent];
        
        NSURL *imageUrl = [[NSBundle mainBundle] URLForResource:imageName withExtension:extension subdirectory:subdirectory];
        
        // Substitute URL into string
        [processedHtml appendString:[imageUrl absoluteString]];
        
        lastMatchIndex = result.range.location + result.range.length;
    }];
    [processedHtml appendString:[html substringWithRange:NSMakeRange(lastMatchIndex, [html length] - lastMatchIndex)]];
    
    return processedHtml;
}

#pragma mark - Table view delegate

- (NSString *)aboutUsHTML
{
    return [self processHtml:[@[
                              @"<a href='http://www.planticle.com.au'><img src='bundle:images/planticle.png'></a>",
                              @"<p><br/>Park Play was conceived, built and published by Planticle in Adelaide, Australia.</p>",
                              @"<p>We are a small family-owned and operated software development business and specialists in creating stunning mobile apps.</p>",
                              @"<p>Interested in producing an app for yourself or your business? Contact us today for a no-obligation discussion and quote - <a href='http://www.planticle.com.au'>visit our website</a> for more information.</p>",
                              @"<p><img width='300px' height='220px' src='bundle:images/team.jpg'></p>"
                              ] componentsJoinedByString:@"\n"]];
}

- (NSString *)aboutAppHTML
{
    return [self processHtml:[@[
                              @"<p><b>Welcome to Park Play!</b></p>",
                              [NSString stringWithFormat:@"<p>The Park Play app features a searchable database of %d playgrounds in the Adelaide Metropolitan area.</p>", [PAData sharedInstance].playgrounds.count],
                              @"<p>Each playground has been visited, photographed and audited by us personally to ensure that the data is consistent and of high quality. While every effort has been made to ensure accurate information, please report any issues via the Feedback facility in the About screen.</p>",
                              @"<p><b>Update:</b> We have now partnered with <a href='http://www.kidsaroundtown.com.au/'>Kids Around Town</a> to ensure that playground information in the Park Play app is updated on a regular basis.</p>",
                              @"<p><b>Which playgrounds are included?</b></p>",
                              @"<p>Only outdoor playgrounds that are free and open to the public are included. Parks and reserves that do not contain playground equipment are not included.</p>",
                              @"<p>Coverage includes most of the Adelaide Metropolitan area, with the exception of Gawler, Adelaide Hills and suburbs south of Seacliff. These suburbs will be added shortly, however there are no plans currently to extend coverage beyond the Adelaide Metropolitan area.</p>",
                              @"<p><b>Approach to data collection</b></p>",
                              @"<p>Since opinions on the suitability of playgrounds for specific age ranges, safety, cleanliness etc. varies widely from person to person, we have provided only factual information. From the extensive photos we have provided, you can make your own judgement.</p>",
                              @"<p>You may view a selection of our favourite playgrounds by tapping Favourites on the Home screen, then selecting Park Play's Favourites.</p>"
                              @"<p><b>A note on toilet facilities</b></p>",
                              @"<p>At some playgrounds toilet facilities were present but locked at the time we visited them (within business hours). In this case, we recorded the presence of toilets but included a photo of the locked toilet entrance to indicate that the toilet may not be open.</p>"
                              ] componentsJoinedByString:@"\n"]];
}

- (NSString *)acknowledgementsHTML
{
    return [self processHtml:[@[
                              @"<p>A big thank you goes to:<br/><br/><b>Blue River Design</b> for creating our brand identity, and<br/><br/><b>Hannah Redding</b> for creating some of the icon graphics.<br/></p>",
                              @"<p>We would also like to thank the following councils for providing park and reserve lists:</p>",
                              @"<p>"
                              @"City of Port Adelaide Enfield<br>",
                              @"City of Tea Tree Gully<br>",
                              @"City of Charles Sturt<br>",
                              @"City of Mitcham<br>",
                              @"Adelaide City<br>",
                              @"Campbelltown City<br>",
                              @"City of Unley<br>",
                              @"City of West Torrens<br>",
                              @"</p>"
                              ] componentsJoinedByString:@"\n"]];
}

- (NSString *)licenseText
{
    return [@[
            @"DTCoreText",
            @"----------",
            @"This software is based in part on DTCoreText:",
            @"Copyright (c) 2011, Oliver Drobnik All rights reserved.",
            @"",
            @"Redistribution and use in source and binary forms, with or without",
            @"modification, are permitted provided that the following conditions are met:",
            @"",
            @"- Redistributions of source code must retain the above copyright notice, this",
            @"  list of conditions and the following disclaimer. ",
            @"",
            @"- Redistributions in binary form must reproduce the above copyright notice,",
            @"  this list of conditions and the following disclaimer in the documentation",
            @"  and/or other materials provided with the distribution.",
            @"",
            @"THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS \"AS IS\"",
            @"AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE",
            @"IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE",
            @"DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE",
            @"FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL",
            @"DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR",
            @"SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER",
            @"CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,",
            @"OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE",
            @"OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.",
            @"",
            @"Cocoanetics is the original author of DTCoreText",
            @"",
            [GMSServices openSourceLicenseInfo]] componentsJoinedByString:@"\n"];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITextView *textView;
    DTAttributedTextView *attrTextView;
    UIActionSheet *actionSheet;
    UIColor *bgColor = [PAUtil yellow];
    NSData *htmlData;
    NSString *title;
    
    switch((AboutSectionType)indexPath.section) {
        case kAbout:
            switch((AboutRowType)indexPath.row) {
                case kAboutUs:
                    htmlData = [[self aboutUsHTML] dataUsingEncoding:NSUTF8StringEncoding];
                    title = @"About Us";
                    break;
                case kAboutApp:
                    htmlData = [[self aboutAppHTML] dataUsingEncoding:NSUTF8StringEncoding];
                    title = @"About This App";
                    break;
                case kAcknowledgements:
                    htmlData = [[self acknowledgementsHTML] dataUsingEncoding:NSUTF8StringEncoding];
                    title = @"Acknowledgements";
                    break;
            }
            
            attrTextView = [[DTAttributedTextView alloc] initWithFrame:CGRectZero];
            attrTextView.attributedString = [[NSAttributedString alloc] initWithHTMLData:htmlData
                                                                                 options:@{
                                                                     DTDefaultFontFamily: @"American Typewriter",
                                                                       DTDefaultFontSize: [NSNumber numberWithFloat:16.f]}
                                                                      documentAttributes:nil];
            attrTextView.backgroundColor = bgColor;
            attrTextView.contentInset = UIEdgeInsetsMake(10.f, 10.f, 10.f, 10.f);
            attrTextView.textDelegate = self;
            [self pushView:attrTextView withTitle:title];
            
            break;
        case kFeedback:
            switch((FeedbackRowType)indexPath.row) {
                case kCorrection:
                    destinationURL = @"mailto:goplay@planticle.com.au?subject=Incorrect Information in goPlay App";
                    break;
                case kSuggestion:
                    destinationURL = @"mailto:goplay@planticle.com.au?subject=Playground Suggestions for goPlay App";
                    break;
            }
            actionSheet = [[UIActionSheet alloc] initWithTitle:@"Send Feedback" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Send Email", nil];
            [actionSheet showInView:self.view.superview];
            break;
        case kInfo:
            switch((InfoRowType)indexPath.row) {
                case kVersion:
                    break;
                case kLegal:
                    textView = [[UITextView alloc] init];
                    textView.text = [NSString stringWithFormat:@"\n%@", [self licenseText]];
                    textView.editable = FALSE;
                    textView.textColor = [UIColor darkTextColor];
                    textView.font = [UIFont fontWithName:@"AmericanTypewriter" size:12.f];
                    textView.backgroundColor = bgColor;
                    [self pushView:textView withTitle:@"Legal Notices"];
                    break;
                case kNumberOfPlaygrounds:
                    break;
            }
            break;
    }
}

- (UIView *)attributedTextContentView:(DTAttributedTextContentView *)attributedTextContentView
                          viewForLink:(NSURL *)url
                           identifier:(NSString *)identifier
                                frame:(CGRect)frame
{
    DTLinkButton *linkButton = [[DTLinkButton alloc] initWithFrame:frame];
    linkButton.URL = url;
    [linkButton addTarget:self
                   action:@selector(didPressLink:)
         forControlEvents:UIControlEventTouchUpInside];
    
    return linkButton;
}

- (IBAction)didPressLink:(DTLinkButton *)button
{
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:[button.URL absoluteString] delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Visit Website", nil];
    actionSheet.tag = 1;
    destinationURL = [button.URL absoluteString];
    [actionSheet showInView:self.navigationController.view];
}

- (void) pushView:(UIView *)view withTitle:(NSString *)title
{
    UIViewController *controller = [[UIViewController alloc] init];
    controller.view = view;
    controller.title = title;
    controller.navigationItem.leftBarButtonItem = [PAUtil backButtonWithTarget:self action:@selector(didPressBackButton)];
    [self.navigationController pushViewController:controller animated:YES];
}

- (void) didPressBackButton
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - Action sheet delegate

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(buttonIndex == 0) {
        [[UIApplication sharedApplication] openURL:[[NSURL alloc] initWithString:[destinationURL stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]]];
    }
}

- (void)actionSheet:(UIActionSheet *)actionSheet willDismissWithButtonIndex:(NSInteger)buttonIndex
{
    [self.tableView deselectRowAtIndexPath:[self.tableView indexPathForSelectedRow] animated:YES];
}

@end
