//
//  PAMainViewController.h
//  PlaygroundApp
//
//  Created by James Watmuff on 27/02/13.
//  Copyright (c) 2013 Planticle. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PASortViewController.h"
#import "PAFilterViewController.h"
#import "PAAboutViewController.h"
#import "Settings.h"

@class PAListViewController;

@interface PAMainViewController : UIViewController <PASortViewControllerDelegate, PAFilterViewControllerDelegate, PAAboutViewControllerDelegate, CLLocationManagerDelegate> {
}

- (void)showMap;
- (void)showList;

@property(strong,nonatomic) Settings *settings;

@end
