//
//  PAMainToolbar.h
//  PlaygroundApp
//
//  Created by James Watmuff on 25/07/13.
//  Copyright (c) 2013 Planticle. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PAMainToolbar : UIView

- (void) setVisibility:(BOOL)visibility ofButton:(UIButton *)button;

@property (nonatomic, strong) UIButton *sortButton;
@property (nonatomic, strong) UIButton *filterButton;
@property (nonatomic, strong) UISegmentedControl *mapListToggle;

@end
