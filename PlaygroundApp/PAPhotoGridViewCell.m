//
//  PAPhotoGridViewCell.m
//  PlaygroundApp
//
//  Created by James Watmuff on 7/03/13.
//  Copyright (c) 2013 Planticle. All rights reserved.
//

#import "PAPhotoGridViewCell.h"
#import "UIImageView+AFNetworking.h"

@implementation PAPhotoGridViewCell

- (id) initWithFrame: (CGRect) frame reuseIdentifier: (NSString *) aReuseIdentifier
{
    self = [super initWithFrame: frame reuseIdentifier: aReuseIdentifier];
    if ( self == nil )
        return ( nil );
    
    _imageView = [[UIImageView alloc] initWithFrame: CGRectZero];
    _ribbonView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"images/awesome"]];
    [self.contentView addSubview: _imageView];
    
    self.backgroundColor = nil;
    self.contentView.backgroundColor = nil;
    
    return ( self );
}


- (CALayer *) glowSelectionLayer
{
    return ( _imageView.layer );
}

- (UIImage *) image
{
    return ( _imageView.image );
}

- (void) setImage: (UIImage *) anImage
{
    _imageView.image = anImage;
    
    [self setNeedsLayout];
}

- (void) setImageURL: (NSURL *) url
{
    __weak typeof(self) weakSelf = self;
    
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    [_imageView setImageWithURLRequest:request
                      placeholderImage:[UIImage imageNamed:@"images/placeholder"]
                               success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image) {
                                   //NSLog(@"Downloaded %@", url);
                                   [weakSelf setNeedsLayout];
                               }
                                failure:nil];
}

- (void) setRibbonDisplayed:(BOOL)ribbonDisplayed
{
    if(ribbonDisplayed) {
        [self.contentView addSubview: _ribbonView];
        [self.contentView bringSubviewToFront:_ribbonView];
    } else {
        [_ribbonView removeFromSuperview];
    }
}

- (void) layoutSubviews
{
    [super layoutSubviews];
    
    CGSize imageSize = _imageView.image.size;
    CGRect frame = _imageView.frame;
    CGRect bounds = self.contentView.bounds;
    
    if(imageSize.width <= 0 || imageSize.height <= 0) return;
    
    // scale it down to fit
    CGFloat hRatio = bounds.size.width / imageSize.width;
    CGFloat vRatio = bounds.size.height / imageSize.height;
    CGFloat ratio = MAX(hRatio, vRatio);
    
    frame.size.width = floorf(imageSize.width * ratio);
    frame.size.height = floorf(imageSize.height * ratio);
    frame.origin.x = floorf((bounds.size.width - frame.size.width) * 0.5);
    frame.origin.y = floorf((bounds.size.height - frame.size.height) * 0.5);
    _imageView.frame = frame;
}

@end