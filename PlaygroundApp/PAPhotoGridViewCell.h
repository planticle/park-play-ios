//
//  PAPhotoGridViewCell.h
//  PlaygroundApp
//
//  Created by James Watmuff on 7/03/13.
//  Copyright (c) 2013 Planticle. All rights reserved.
//

#import "AQGridViewCell.h"

@interface PAPhotoGridViewCell : AQGridViewCell
{
    UIImageView * _imageView;
    UIImageView * _ribbonView;
}
@property (nonatomic, retain) UIImage * image;

- (void) setImageURL: (NSURL *) url;
- (void) setRibbonDisplayed: (BOOL)ribbonDisplayed;

@end
