//
//  PAFilterViewController.h
//  PlaygroundApp
//
//  Created by James Watmuff on 5/03/13.
//  Copyright (c) 2013 Planticle. All rights reserved.
//

#import <UIKit/UIKit.h>

@class PAFilterViewController;

@protocol PAFilterViewControllerDelegate
- (void)filterViewControllerDidFinish:(PAFilterViewController *)controller;
@end

@interface PAFilterViewController : UITableViewController

@property (nonatomic, strong) NSMutableSet *selectedFacilities;
@property (nonatomic, strong) NSMutableSet *selectedEquipment;
@property (nonatomic, assign) BOOL favouriteSelected;
@property (nonatomic, assign) BOOL ourFavouriteSelected;
@property (nonatomic, weak) id <PAFilterViewControllerDelegate> delegate;

@end
