//
//  PAMapViewController.m
//  PlaygroundApp
//
//  Created by James Watmuff on 27/02/13.
//  Copyright (c) 2013 Planticle. All rights reserved.
//

#import "PAMapViewController.h"
#import "PADetailInfoViewController.h"
#import "PAUtil.h"

#import "PAPointIndex.h"
#import "PAAsyncPointIndex.h"
#import "PADataImporter.h"

#import "PlaygroundFacility.h"
#import "PlaygroundEquipment.h"
#import "Playground+Extra.h"
#import <GoogleMaps/GoogleMaps.h>
#import <YIInnerShadowView.h>

static const int DETAIL_HEIGHT = 40;
static const int NUMBER_OF_ICONS_PORTRAIT = 9;
static const int NUMBER_OF_ICONS_LANDSCAPE = 14;
static const int DEFAULT_ZOOM = 14;

@interface PAMapViewController () {
    GMSMapView *mapView;
    NSSet *lastSelectedFacilities;
    NSSet *lastSelectedEquipment;
    BOOL lastFavouriteSelected;
    BOOL lastOurFavouriteSelected;
    Playground *selectedPlayground;
    int mapMoves;
    UIView *detailView;
    
    NSTimer *mapActivityTimer;
    NSMutableArray *allMarkers;
    PAAsyncPointIndex *pointIndexAsync;
}

@end

@implementation PAMapViewController

@synthesize mainViewController;
@synthesize settings;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        selectedPlayground = nil;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    GMSCameraPosition *camera;
    
    // Get an initial location - probably cached. If it is less than 2 days old, use it
    // as the initial center for the map. Otherwise, use a default initial location.
    CLLocation *initialLocation = [[CLLocationManager alloc] init].location;
    if(initialLocation && [initialLocation.timestamp timeIntervalSinceNow] > -(2 * 24 * 60 * 60)) {
        camera = [GMSCameraPosition cameraWithTarget:initialLocation.coordinate zoom:DEFAULT_ZOOM];
    } else {
        camera = [GMSCameraPosition cameraWithLatitude:-34.9333 longitude:138.5833 zoom:9];
    }
    
    mapView = [GMSMapView mapWithFrame:self.view.bounds camera:camera];
    mapView.settings.compassButton = YES;
    mapView.settings.myLocationButton = YES;

    // Rotate is too slow on old devices
    if([PAUtil numberOfCores] < 2) mapView.settings.rotateGestures = NO;
    
    mapView.myLocationEnabled = YES;
    mapView.delegate = self;
    
    mapView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    
    [self.view addSubview:mapView];

    CLLocationManager *locationManager = [[CLLocationManager alloc] init];
    locationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters;
    locationManager.delegate = self;
    [locationManager startUpdatingLocation];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(dataWasUpdated) name:@"DataWasUpdated" object:nil];
}

- (void)dataWasUpdated
{
    selectedPlayground = nil;
    settings.selectedEquipment = [[NSMutableSet alloc] init];
    settings.selectedFacilities = [[NSMutableSet alloc] init];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated
{
    CGRect frame = self.parentViewController.view.bounds;

    // hack to account for additional UIToolbar
    frame.origin.y += 44;
    frame.size.height -=44;

    self.view.frame = frame;

    [super viewWillAppear:animated];

    [self loadData];
}

- (void)viewWillLayoutSubviews
{
    // iOS 7 Adjustments
    if([[[UIDevice currentDevice] systemVersion] doubleValue] >= 7) {
        CGRect frame = self.parentViewController.view.bounds;
        id<UILayoutSupport> topLayoutGuide = [self.parentViewController topLayoutGuide];
        frame.origin.y += ([topLayoutGuide length] + 44);
        frame.size.height -= ([topLayoutGuide length] + 44);
        self.view.frame = frame;
    }
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    if(![[NSUserDefaults standardUserDefaults] boolForKey:@"mapTipDisplayed"]) {
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"mapTipDisplayed"];
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Handy Tip"
                                                        message:@"Tap and hold anywhere on the map to view a list of playgrounds near that point."
                                                       delegate:nil
                                              cancelButtonTitle:@"Got it, Thanks"
                                              otherButtonTitles:nil];
        [alert show];
    }
}

- (void)loadData
{
    [mapView clear];
    
    Playground *oldSelectedPlayground = selectedPlayground;
    selectedPlayground = nil;

    // --- Get all playgrounds
    NSSet *playgrounds = [settings selectPlaygrounds];
    
    
    // --- Set up point index for fast marker rendering
    if(pointIndexAsync) pointIndexAsync.delegate = nil;
    pointIndexAsync = [[PAAsyncPointIndex alloc] init];
    pointIndexAsync.delegate = self;
    
    // --- Create marker for each
    allMarkers = [[NSMutableArray alloc] init];
    for(Playground *playground in playgrounds) {
        if([playground.lat isEqualToNumber:@0.0]) continue;
        
        GMSMarker *marker = [GMSMarker markerWithPosition:CLLocationCoordinate2DMake(playground.lat.doubleValue, playground.lon.doubleValue)];
        marker.position = CLLocationCoordinate2DMake(playground.lat.doubleValue, playground.lon.doubleValue);
        marker.title = playground.name;
        marker.snippet = playground.address;
        marker.userData = playground;
        marker.icon = [UIImage imageNamed:(playground.favourite.boolValue ? @"images/pin_favourite" : @"images/pin")];
        //marker.map = mapView;

        if(playground == oldSelectedPlayground) {
            mapView.selectedMarker = marker;
            marker.map = mapView;
            selectedPlayground = playground;
        }
        
        [pointIndexAsync addCoordinate:marker.position withUserData:marker];
        [allMarkers addObject:marker];
    }
    
    if([self filterSettingsHaveChanged]) {
        [self ensureClosestPlaygroundIsVisibleForPlaygrounds:playgrounds];
    }
    
    if(!selectedPlayground) {
        [self hideDetailView];
    }
    
    [self showMarkersInVisibleExtent];
}

- (BOOL) filterSettingsHaveChanged
{
    BOOL same = [settings.selectedFacilities isEqualToSet:lastSelectedFacilities] && [settings.selectedEquipment isEqualToSet:lastSelectedEquipment] && (settings.favouriteSelected == lastFavouriteSelected) && (settings.ourFavouriteSelected == lastOurFavouriteSelected);
    lastSelectedFacilities = [[NSSet alloc] initWithSet:settings.selectedFacilities];
    lastSelectedEquipment = [[NSSet alloc] initWithSet:settings.selectedEquipment];
    lastFavouriteSelected = settings.favouriteSelected;
    lastOurFavouriteSelected = settings.ourFavouriteSelected;
    return !same;
}

- (void) ensureClosestPlaygroundIsVisibleForPlaygrounds:(NSSet *)playgrounds
{
    if(playgrounds.count == 0) return;
    
        
    GMSCameraPosition *camera = mapView.camera;
    CLLocation *center = [[CLLocation alloc] initWithLatitude:camera.target.latitude longitude:camera.target.longitude];
    Playground *closest = [[PAUtil sortPlaygrounds:[playgrounds allObjects] byDistanceFromLocation:center] objectAtIndex:0];
    
    if(![mapView.projection containsCoordinate:closest.location.coordinate]) {
        GMSCoordinateBounds *bounds = [[GMSCoordinateBounds alloc] initWithCoordinate:center.coordinate coordinate:closest.coordinate];
        GMSCameraUpdate *update = [GMSCameraUpdate fitBounds:bounds withPadding:100];
        [mapView moveCamera:update];
    } else {
        //NSLog(@"Closest playground (%@) is visible", closest.name);
    }
}

- (void) showMarkersInVisibleExtent
{
    GMSVisibleRegion visibleRegion = mapView.projection.visibleRegion;

    [pointIndexAsync updateWithExtentOfCoordinate:visibleRegion.nearLeft
                                       coordinate:visibleRegion.nearRight
                                       coordinate:visibleRegion.farLeft
                                       coordinate:visibleRegion.farRight];
}

- (void) pointIndex:(PAAsyncPointIndex *)pointIndex hasUpdatedItemsToAdd:(NSMutableArray *)itemsToAdd andRemove:(NSMutableArray *)itemsToRemove
{
    for(GMSMarker *marker in itemsToRemove) {
        if(mapView.selectedMarker != marker) marker.map = nil;
    }
    for(GMSMarker *marker in itemsToAdd) marker.map = mapView;
}

- (void)selectPlayground:(Playground *)playground
{
    selectedPlayground = playground;
    for(GMSMarker *marker in allMarkers)
    {
        if([marker userData] == playground) {
            marker.map = mapView;
            mapView.selectedMarker = marker;
            [mapView animateToLocation:playground.coordinate];
            [self showDetailViewForPlayground:playground];
        }
    }
}

#pragma mark - CLLocationManagerDelegate

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    if(mapMoves < 5) {
        CLLocation *location = locations.lastObject;
        GMSCameraPosition *camera = [GMSCameraPosition cameraWithTarget:location.coordinate zoom:DEFAULT_ZOOM];
        [mapView animateToCameraPosition:camera];
    }
    [manager stopUpdatingLocation];
}


#pragma mark - GMSMapViewDelegate

/**
 * Called after a marker's info window has been tapped.
 */
- (void)mapView:(GMSMapView *)mapView didTapInfoWindowOfMarker:(GMSMarker *)marker
{
    Playground *playground = (Playground *)marker.userData;

    [self showDetailsForPlayground:playground];
}

- (BOOL)mapView:(GMSMapView *)aMapView didTapMarker:(GMSMarker *)marker
{
    selectedPlayground = [marker userData];
    
    // Hack - determines whether info window is appearing or disappearing as a result of the
    // current tap.
    BOOL closingInfoWindow = (marker == aMapView.selectedMarker);

    if(closingInfoWindow) {
        [self hideDetailView];
        selectedPlayground = nil;
    }
    else {
        [self showDetailViewForPlayground:selectedPlayground];
    }
    return NO;
}

- (void)mapView:(GMSMapView *)mapView didTapAtCoordinate:(CLLocationCoordinate2D)coordinate
{
    [self hideDetailView];
    selectedPlayground = nil;
}

-(void)mapView:(GMSMapView *)aMapView didChangeCameraPosition:(GMSCameraPosition *)position
{
    mapMoves++;
    [self showMarkersInVisibleExtent];
    if(mapActivityTimer) {
        [mapActivityTimer invalidate];
    }
    //mapActivityTimer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(mapWentQuiet) userInfo:nil repeats:NO];
}

- (void)mapView:(GMSMapView *)aMapView didLongPressAtCoordinate:(CLLocationCoordinate2D)coordinate
{
    GMSMarker *marker = [GMSMarker markerWithPosition:coordinate];
    marker.icon = [UIImage imageNamed:@"images/pin_drop"];
    marker.map = mapView;
    [mapView animateToLocation:coordinate];
    
    UIActionSheet *sheet = [[UIActionSheet alloc] initWithTitle:@"Map Location Selected" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Show Nearby Playgrounds", nil];
    [sheet showInView:self.navigationController.view];
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(buttonIndex == 0) {
        [settings reset];
        settings.sortPin = mapView.camera.target;
        settings.sortMode = kDistanceFromPin;
        [mainViewController showList];
    } else {
        [self loadData];
    }
}

# pragma mark - Custom detail view

-(void)mapWentQuiet
{
    mapActivityTimer = nil;
    [self showMarkersInVisibleExtent];
}

-(void)showDetailsForPlayground:(Playground *)playground
{
    PADetailInfoViewController *detailViewController = [[PADetailInfoViewController alloc] initWithStyle:UITableViewStyleGrouped];
    detailViewController.playground = playground;
    
    [self.mainViewController.navigationController pushViewController:detailViewController animated:YES];
}

-(void)handleDetailViewTap:(id)sender
{
    [self showDetailsForPlayground:selectedPlayground];
}

-(void)createDetailView
{
    YIInnerShadowView *view = [[YIInnerShadowView alloc] initWithFrame:CGRectMake(0, self.view.bounds.size.height - 0, self.view.bounds.size.width, DETAIL_HEIGHT)];
    view.shadowMask = YIInnerShadowMaskTop;
    view.shadowRadius = 5;
    view.autoresizingMask = UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleWidth;
    view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"images/bg"]];
    
    int i;
    for(i = 0; i < NUMBER_OF_ICONS_LANDSCAPE; i++) {
        UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(10 + i * 32, 10, 25, 25)];
        imageView.tag = i+1;
        [view addSubview:imageView];
    }

    UIImageView *arrow = [[UIImageView alloc] initWithFrame:CGRectMake(self.view.bounds.size.width - 20, 12.5, 12.5, 20)];
    arrow.image = [UIImage imageNamed:@"images/chevron"];
    [view addSubview:arrow];
    arrow.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin;
    
    UITapGestureRecognizer *gestureRecogniser = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleDetailViewTap:)];
    [view addGestureRecognizer:gestureRecogniser];


    detailView = view;
}

-(void)showDetailViewIconsForOrientation:(UIInterfaceOrientation)orientation
{
    if(!detailView) return;
    for(int i = NUMBER_OF_ICONS_PORTRAIT; i < NUMBER_OF_ICONS_LANDSCAPE; i++) {
        UIView *view = [detailView viewWithTag:i+1];
        view.hidden = !UIInterfaceOrientationIsLandscape(orientation);
    }
}

-(void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    [self showDetailViewIconsForOrientation:toInterfaceOrientation];
}

-(void)showDetailViewForPlayground:(Playground *)playground
{
    if(detailView == nil) {
        [self createDetailView];
    }

    // populate facility icons
    int i = 0;
    NSArray *facilities = [playground.facilities sortedArrayUsingDescriptors:@[[NSSortDescriptor sortDescriptorWithKey:@"facility.name" ascending:YES]]];
    for(PlaygroundFacility *pf in facilities) {
        if(i >= NUMBER_OF_ICONS_LANDSCAPE) break;
        UIImageView *iconView = (UIImageView *)[detailView viewWithTag:i+1];
        iconView.image = [PAUtil iconForFacility:pf.facility];
        i++;
    }
    
    NSArray *equipment = [playground.equipment sortedArrayUsingDescriptors:@[[NSSortDescriptor sortDescriptorWithKey:@"equipment.name" ascending:YES]]];
    for(PlaygroundEquipment *pe in equipment) {
        if(i >= NUMBER_OF_ICONS_LANDSCAPE) break;
        UIImageView *iconView = (UIImageView *)[detailView viewWithTag:i+1];
        iconView.image = [PAUtil iconForEquipment:pe.equipment];
        i++;
    }
    
    while(i < NUMBER_OF_ICONS_LANDSCAPE) {
        UIImageView *iconView = (UIImageView *)[detailView viewWithTag:i+1];
        iconView.image = nil;
        i++;
    }
    
    [self showDetailViewIconsForOrientation:self.interfaceOrientation];
    
    if(!detailView.superview) {
        detailView.frame = CGRectMake(0, self.view.bounds.size.height - 0, self.view.bounds.size.width, DETAIL_HEIGHT);
        [self.view addSubview:detailView];
        [UIView animateWithDuration:0.5 animations:^{
            detailView.frame = CGRectOffset(detailView.frame, 0, -DETAIL_HEIGHT);
            mapView.frame = CGRectIntersection(mapView.frame, CGRectOffset(mapView.frame, 0, -DETAIL_HEIGHT));
        }];
    }
}

-(void)hideDetailView
{
    if(detailView && detailView.superview) {
        [UIView animateWithDuration:0.5 animations:^{
            detailView.frame = CGRectOffset(detailView.frame, 0, DETAIL_HEIGHT);
            mapView.frame = CGRectUnion(mapView.frame, CGRectOffset(mapView.frame, 0, DETAIL_HEIGHT));
        } completion:^(BOOL finished){
            [detailView removeFromSuperview];
        }];
    }
}
@end
