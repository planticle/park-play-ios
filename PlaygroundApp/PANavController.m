//
//  PANavController.m
//  PlaygroundApp
//
//  Created by James Watmuff on 27/02/13.
//  Copyright (c) 2013 Planticle. All rights reserved.
//

#import "PANavController.h"

@interface PANavController ()

@end

@implementation PANavController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    //self.navigationBar.tintColor = [UIColor colorWithRed:130/255.0f green:0/255.0f blue:156/255.0f alpha:1];
    //self.navigationBar.titleTextAttributes = @{UITextAttributeFont: [UIFont fontWithName:@"Helvetica-Bold" size:16.0]};
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
