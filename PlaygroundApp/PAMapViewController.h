//
//  PAMapViewController.h
//  PlaygroundApp
//
//  Created by James Watmuff on 27/02/13.
//  Copyright (c) 2013 Planticle. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <GoogleMaps/GoogleMaps.h>
#import "Playground.h"
#import "Settings.h"
#import "PAAsyncPointIndex.h"
#import "PAMainViewController.h"

@interface PAMapViewController : UIViewController <GMSMapViewDelegate, CLLocationManagerDelegate, PAAsyncPointIndexDelegate, UIActionSheetDelegate>

@property (nonatomic, strong) PAMainViewController *mainViewController;
@property (nonatomic, strong) Settings *settings;

- (void)selectPlayground:(Playground *)playground;

@end
