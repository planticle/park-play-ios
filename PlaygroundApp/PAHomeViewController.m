//
//  PAHomeViewController.m
//  PlaygroundApp
//
//  Created by James Watmuff on 30/07/13.
//  Copyright (c) 2013 Planticle. All rights reserved.
//

#import "PAHomeViewController.h"
#import "PAMainViewController.h"
#import "PAUtil.h"

@interface PAHomeViewController () {
    UIImageView *imageView;
    PAMainViewController *mainViewController;
}

@end

@implementation PAHomeViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    self.navigationController.navigationBarHidden = YES;
    
    BOOL widescreen = [UIScreen mainScreen].bounds.size.height >= 568;
    
    NSString *imageName = widescreen ? @"images/home_bg-568h" : @"images/home_bg";
    imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:imageName]];
    [self.view addSubview:imageView];
    
    // iOS 7 status bar adjustment
    if([[[UIDevice currentDevice] systemVersion] doubleValue] >= 7) {
        UIImageView *statusBackground = [[UIImageView alloc] initWithImage:[PAUtil imageWithColor:[PAUtil yellow] size:CGSizeMake(self.view.frame.size.width, 20)]];
        [self.view addSubview:statusBackground];
        imageView.frame = CGRectOffset(imageView.frame, 0, 20);
    }
    
    NSArray *buttonTitles = @[
                             @"Select by map",
                             @"Find parks near me",
                             @"Favourites"
                             ];
    
    int startY = imageView.frame.origin.y + imageView.bounds.size.height;
    int buttonHeight = 54;
    int buttonWidth = self.view.bounds.size.width;
    UIImage *buttonBackground = [PAUtil imageWithColor:[UIColor colorWithRed:142.f/255.f green:207.f/255.f blue:182.f/255.f alpha:1]
                                           borderColor:[UIColor colorWithRed:95.f/255.f green:137.f/255.f blue:123.f/255.f alpha:1]
                                                  size:CGSizeMake(1, buttonHeight)
                                          borderInsets:UIEdgeInsetsMake(0, 0, 4, 0)];
    
    int i = 0;
    for(NSString *title in buttonTitles) {
        UIButton *button = [[UIButton alloc] initWithFrame:CGRectMake(0,startY + i * buttonHeight, buttonWidth, buttonHeight)];
        [button setBackgroundImage:buttonBackground forState:UIControlStateNormal];
        [button setTitle:title forState:UIControlStateNormal];
        [button addTarget:self action:@selector(didPressButton:) forControlEvents:UIControlEventTouchUpInside];
        [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        button.titleLabel.font = [UIFont fontWithName:@"AmericanTypewriter" size:22.f];
        button.tag = i;
        i++;
        [self.view addSubview:button];
    }

    self.view.backgroundColor = [UIColor colorWithRed:126.f/255.f green:184.f/255.f blue:164.f/255.f alpha:1];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self.navigationController setNavigationBarHidden:YES animated:animated];
}

- (void)aboutViewControllerDidFinish:(PAAboutViewController *)controller
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)didPressAboutButton
{
    PAAboutViewController *aboutViewController = [[PAAboutViewController alloc] initWithStyle:UITableViewStyleGrouped];
    aboutViewController.delegate = self;
    
    UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:aboutViewController];
    navController.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
    navController.navigationBar.tintColor = self.navigationController.navigationBar.tintColor;
    
    [self presentViewController:navController animated:YES completion:nil];

}

- (void)didPressButton:(UIButton *)button
{
    if(mainViewController == nil) mainViewController = [[PAMainViewController alloc] init];
    [mainViewController.settings reset];
    UIActionSheet *actionSheet;

    switch(button.tag) {
        case 0:
            [mainViewController showMap];
            [self showMainView];
            break;
        case 1:
            [mainViewController showList];
            [self showMainView];
            break;
        case 2:
            actionSheet = [[UIActionSheet alloc] initWithTitle:@"View Favourite Playgrounds"
                                                      delegate:self
                                             cancelButtonTitle:@"Cancel"
                                        destructiveButtonTitle:nil
                                             otherButtonTitles:@"My Favourites", @"Park Play's Favourites", nil];
            [actionSheet showInView:self.view];
            return;
        default:
            NSLog(@"Unexpected tag in didPressButton: %d", button.tag);
            return;
    }
    
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    switch(buttonIndex) {
        case 0:
        case 1:
            mainViewController.settings.favouriteSelected = (buttonIndex == 0);
            mainViewController.settings.ourFavouriteSelected = (buttonIndex == 1);
            [mainViewController showList];
            [self showMainView];
            break;
    }
}

- (void)showMainView
{
    CATransition *transition;
    transition = [CATransition animation];
    transition.duration = 0.4;
    transition.type = kCATransitionReveal;
    transition.subtype = kCATransitionFromTop;
    [self.navigationController.view.layer addAnimation:transition forKey:kCATransition];
    
    [self.navigationController pushViewController:mainViewController animated:NO];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
